﻿<?xml version="1.0" encoding="utf-8"?>
<Configuration>
  <Element ID="gAlarmXCore_CFG" Type="mpalarmxcore">
    <Group ID="mapp.AlarmX.Core">
      <Group ID="Mapping">
        <Group ID="[0]" />
      </Group>
      <Group ID="BySeverity">
        <Group ID="[0]">
          <Property ID="Severity" Value="1" />
          <Selector ID="[0]" Value="Reaction">
            <Property ID="Name" Value="STOP_MACHINE" />
          </Selector>
          <Selector ID="[1]" Value="Reaction">
            <Property ID="Name" Value="CONTROL_OFF" />
          </Selector>
        </Group>
        <Group ID="[1]">
          <Property ID="Severity" Value="2" />
          <Selector ID="[0]" Value="Reaction">
            <Property ID="Name" Value="STOP_MACHINE" />
          </Selector>
        </Group>
        <Group ID="[2]">
          <Property ID="Severity" Value="3" />
          <Selector ID="[0]" Value="Reaction">
            <Property ID="Name" Value="MESSAGE" />
          </Selector>
        </Group>
      </Group>
    </Group>
    <Group ID="mapp.AlarmX.Core.Configuration">
      <Group ID="[0]">
        <Property ID="Name" Value="Sev1Test" />
        <Property ID="Message" Value="STOP_MACHINE and CONTROL_OFF reactions" />
        <Property ID="Code" Value="1" />
        <Selector ID="Behavior" Value="PersistentAlarm" />
      </Group>
      <Group ID="[1]">
        <Property ID="Name" Value="Sev2Test" />
        <Property ID="Message" Value="STOP_MACHINE reactions" />
        <Property ID="Code" Value="2" />
        <Property ID="Severity" Value="2" />
        <Selector ID="Behavior" Value="PersistentAlarm" />
      </Group>
      <Group ID="[2]">
        <Property ID="Name" Value="Sev3Test" />
        <Property ID="Message" Value="MESSAGE reaction" />
        <Property ID="Code" Value="3" />
        <Property ID="Severity" Value="3" />
        <Selector ID="Behavior" Value="PersistentAlarm" />
      </Group>
      <Group ID="[3]">
        <Property ID="Name" Value="SafetyFault" />
        <Property ID="Message" Value="Safety relay broken! Release Estop, clear light curtains, and wait for CONTROL POWER ON to be enabled." />
        <Property ID="Code" Value="1000" />
        <Selector ID="Behavior" Value="PersistentAlarm">
          <Property ID="Acknowledge" Value="0" />
        </Selector>
        <Property ID="AdditionalInformation1" Value="" />
        <Property ID="AdditionalInformation2" Value="" />
      </Group>
      <Group ID="[4]">
        <Property ID="Name" Value="LightCurtainFault" />
        <Property ID="Message" Value="Light Curtain {&amp;LCNumber} has been tripped!" />
        <Property ID="Code" Value="1001" />
        <Selector ID="Behavior" Value="PersistentAlarm">
          <Property ID="Acknowledge" Value="0" />
        </Selector>
      </Group>
      <Group ID="[5]">
        <Property ID="Name" Value="VFDNotFound" />
        <Property ID="Message" Value="No VFD detected! See alarm details for more information!" />
        <Property ID="Code" Value="1010" />
        <Selector ID="Behavior" Value="PersistentAlarm">
          <Group ID="Recording">
            <Property ID="UnacknowledgedToAcknowledged" Value="FALSE" />
          </Group>
        </Selector>
        <Property ID="AdditionalInformation1" Value="Make sure PLK cable to drive is plugged and node number is set correctly. If using an ABB Drive, configure the following parameters from the hand terminal: 50.01, 51.03, 51.27" />
      </Group>
      <Group ID="[6]">
        <Property ID="Name" Value="VFDFault" />
        <Property ID="Message" Value="VFD is reporting a fault! {&amp;VFDErrorText}" />
        <Property ID="Code" Value="1011" />
        <Selector ID="Behavior" Value="PersistentAlarm">
          <Group ID="Recording">
            <Property ID="AcknowledgedToUnacknowledged" Value="FALSE" />
          </Group>
        </Selector>
      </Group>
      <Group ID="[7]">
        <Property ID="Name" Value="CoolantOL" />
        <Property ID="Message" Value="Coolant pump overloaded!" />
        <Property ID="Code" Value="1020" />
        <Selector ID="Behavior" Value="PersistentAlarm">
          <Group ID="Recording">
            <Property ID="UnacknowledgedToAcknowledged" Value="FALSE" />
          </Group>
        </Selector>
      </Group>
      <Group ID="[8]">
        <Property ID="Name" Value="BlowerMotorOL" />
        <Property ID="Message" Value="Blower motor overloaded!" />
        <Property ID="Code" Value="1021" />
        <Selector ID="Behavior" Value="PersistentAlarm">
          <Group ID="Recording">
            <Property ID="UnacknowledgedToAcknowledged" Value="FALSE" />
          </Group>
        </Selector>
        <Property ID="AdditionalInformation1" Value="" />
      </Group>
      <Group ID="[9]">
        <Property ID="Name" Value="DBOvertemp" />
        <Property ID="Message" Value="DB over temperature!" />
        <Property ID="Code" Value="1022" />
        <Selector ID="Behavior" Value="PersistentAlarm">
          <Group ID="Recording">
            <Property ID="UnacknowledgedToAcknowledged" Value="FALSE" />
          </Group>
        </Selector>
        <Property ID="AdditionalInformation1" Value="" />
        <Property ID="AdditionalInformation2" Value="" />
      </Group>
      <Group ID="[10]">
        <Property ID="Name" Value="IOFault" />
        <Property ID="Message" Value="IO fault! {&amp;IOErrorText}" />
        <Property ID="Code" Value="1023" />
        <Selector ID="Behavior" Value="PersistentAlarm">
          <Property ID="Acknowledge" Value="0" />
          <Group ID="Recording">
            <Property ID="UnacknowledgedToAcknowledged" Value="FALSE" />
          </Group>
        </Selector>
      </Group>
      <Group ID="[11]">
        <Property ID="Name" Value="CoolantFault" />
        <Property ID="Message" Value="Coolant pump did not start when expected!" />
        <Property ID="Code" Value="1024" />
        <Selector ID="Behavior" Value="PersistentAlarm" />
      </Group>
      <Group ID="[12]">
        <Property ID="Name" Value="MotorOvertemp" />
        <Property ID="Message" Value="Motor over temperature!" />
        <Property ID="Code" Value="1025" />
        <Selector ID="Behavior" Value="PersistentAlarm" />
      </Group>
      <Group ID="[13]">
        <Property ID="Name" Value="BlowerFault" />
        <Property ID="Message" Value="The blower motor did not start when the drive powered on!" />
        <Property ID="Code" Value="1026" />
        <Selector ID="Behavior" Value="PersistentAlarm" />
      </Group>
      <Group ID="[14]">
        <Property ID="Name" Value="ModeChangeWhileRunning" />
        <Property ID="Message" Value="Mode changed while running! Machine stopped!" />
        <Property ID="Code" Value="2000" />
        <Property ID="Severity" Value="2" />
        <Selector ID="Behavior">
          <Property ID="MultipleInstances" Value="FALSE" />
          <Group ID="Recording">
            <Property ID="UnacknowledgedToAcknowledged" Value="FALSE" />
          </Group>
        </Selector>
      </Group>
      <Group ID="[15]">
        <Property ID="Name" Value="SendConfigFailed" />
        <Property ID="Message" Value="Error while sending paramters to the drive! Error number {&amp;SendConfigError}, additional info {&amp;SendConfigAddInfo|#x}" />
        <Property ID="Code" Value="2001" />
        <Property ID="Severity" Value="2" />
      </Group>
      <Group ID="[16]">
        <Property ID="Name" Value="CoolantModeChanged" />
        <Property ID="Message" Value="Coolant mode changed while running! Machine stopped!" />
        <Property ID="Code" Value="2002" />
        <Property ID="Severity" Value="2" />
      </Group>
      <Group ID="[17]">
        <Property ID="Name" Value="JogNotAllowed" />
        <Property ID="Message" Value="Jogging only allowed in JOG mode!" />
        <Property ID="Code" Value="3000" />
        <Property ID="Severity" Value="3" />
        <Selector ID="Behavior">
          <Property ID="MultipleInstances" Value="FALSE" />
          <Group ID="Recording">
            <Property ID="UnacknowledgedToAcknowledged" Value="FALSE" />
          </Group>
        </Selector>
      </Group>
      <Group ID="[18]">
        <Property ID="Name" Value="StartNotAllowed" />
        <Property ID="Message" Value="Start button can only be used in RUN mode!" />
        <Property ID="Code" Value="3001" />
        <Property ID="Severity" Value="3" />
        <Selector ID="Behavior">
          <Property ID="MultipleInstances" Value="FALSE" />
          <Group ID="Recording">
            <Property ID="UnacknowledgedToAcknowledged" Value="FALSE" />
          </Group>
        </Selector>
      </Group>
      <Group ID="[19]">
        <Property ID="Name" Value="CoolantRequired" />
        <Property ID="Message" Value="Coolant pump is in REQUIRED mode! Cannot run with coolant pump off!" />
        <Property ID="Code" Value="3002" />
        <Property ID="Severity" Value="3" />
        <Selector ID="Behavior">
          <Property ID="MultipleInstances" Value="FALSE" />
          <Group ID="Recording">
            <Property ID="UnacknowledgedToAcknowledged" Value="FALSE" />
          </Group>
        </Selector>
      </Group>
      <Group ID="[20]">
        <Property ID="Name" Value="MotionPermsReqiured" />
        <Property ID="Message" Value="Not all motion permissives are met! Verify permissives on permissives page." />
        <Property ID="Code" Value="3003" />
        <Property ID="Severity" Value="3" />
      </Group>
      <Group ID="[21]">
        <Property ID="Name" Value="NoMachineConfig" />
        <Property ID="Message" Value="No previous machine configuration was found! A default parameter set has been loaded, ensure it is correct in the CONFIG tab." />
        <Property ID="Code" Value="3004" />
        <Property ID="Severity" Value="3" />
      </Group>
      <Group ID="[22]">
        <Property ID="Name" Value="ControlPowerOnDisabled" />
        <Property ID="Message" Value="CONTROL POWER ON button is no longer disabled." />
        <Property ID="Code" Value="3005" />
        <Selector ID="Behavior" Value="DiscreteValueMonitoring">
          <Property ID="Acknowledge" Value="0" />
          <Group ID="Monitoring">
            <Property ID="MonitoredPV" Value="::Axis:VFDio.Status.PowerRemoval" />
            <Group ID="TriggerValues">
              <Property ID="[0]" Value="TRUE" />
            </Group>
          </Group>
        </Selector>
      </Group>
    </Group>
    <Group ID="mapp.AlarmX.Core.Snippets">
      <Group ID="[0]">
        <Property ID="Key" Value="VFDErrorText" />
        <Selector ID="Value">
          <Property ID="PV" Value="::gAlarmInfo.VFDErrorText" />
        </Selector>
      </Group>
      <Group ID="[1]">
        <Property ID="Key" Value="LCNumber" />
        <Selector ID="Value">
          <Property ID="PV" Value="::gAlarmInfo.LCNumber" />
        </Selector>
      </Group>
      <Group ID="[2]">
        <Property ID="Key" Value="IOErrorText" />
        <Selector ID="Value">
          <Property ID="PV" Value="::gAlarmInfo.IOErrorText" />
        </Selector>
      </Group>
      <Group ID="[3]">
        <Property ID="Key" Value="SendConfigError" />
        <Selector ID="Value">
          <Property ID="PV" Value="::DriveSetup:ConfigErrID" />
        </Selector>
      </Group>
      <Group ID="[4]">
        <Property ID="Key" Value="NodeNumber" />
        <Selector ID="Value">
          <Property ID="PV" Value="::Axis:node" />
        </Selector>
      </Group>
      <Group ID="[5]">
        <Property ID="Key" Value="SendConfigAddInfo" />
        <Selector ID="Value">
          <Property ID="PV" Value="::DriveSetup:ConfigErrAddInfo" />
        </Selector>
      </Group>
    </Group>
  </Element>
</Configuration>