
PROGRAM _INIT

	// Turn Estop Relay off if PLC inits
	gIO.do.EstopRelay := FALSE;

END_PROGRAM

PROGRAM _CYCLIC
	
	 (****** Check ModuleOK on IO Modules ******)
	// Choose which IO modules to monitor
	// gIO.ModulesOK[0] is BC0083
	// gIO.ModulesOK[1] is PS9400
	CASE gMachine.MachineType OF
		ACS880:
			gIO.ModulesOK[4] := TRUE; // ACS880 configuration only uses 2 IO modules
		BRVFD:
		// B&R configuration uses all 3 IO mouldues
	END_CASE
	// Set IOFault
	IF NOT gSimulate THEN
		IF gIO.ModulesOK[0] = FALSE OR gIO.ModulesOK[1] = FALSE OR gIO.ModulesOK[2] = FALSE
			OR gIO.ModulesOK[3] = FALSE OR gIO.ModulesOK[4] = FALSE THEN
			gMachine.Command.UseBtnSim := TRUE;
			gAlarmInfo.PersistentFlags.IOFault := TRUE;
			gAlarmInfo.IOErrorText := 'IO Module unplugged or missing!';
		ELSE
			gMachine.Command.UseBtnSim := FALSE;
			gAlarmInfo.PersistentFlags.IOFault := FALSE;
		END_IF
	ELSE
		gMachine.Command.UseBtnSim := TRUE;
	END_IF;
	
	(****** PSU Monitoring ******)
	IF PSUBusPowerFault THEN
		gAlarmInfo.PersistentFlags.IOFault := TRUE;
		gAlarmInfo.IOErrorText := 'PS9400 bus power fault!';
	END_IF
	IF PSUIOPowerFault THEN
		gAlarmInfo.PersistentFlags.IOFault := TRUE;
		gAlarmInfo.IOErrorText := 'PS9400 IO power fault!';
	END_IF
	
	(***** Simulation Control *****)
	IF gSimulate THEN
		DigitalInput.DBOvertemp			:= TRUE;
		DigitalInput.MotorOvertemp		:= TRUE;
	END_IF
	
	// If using simulated button panel, overwrite IO channels with BtnPanel buttons
	IF gMachine.Command.UseBtnSim THEN
		DigitalInput.MillStart			:= ButtonPanelSim.MillStart;
		DigitalInput.NotMillStop		:= NOT ButtonPanelSim.MillStop;
		DigitalInput.LightCurtain1OK	:= NOT ButtonPanelSim.LightCurtain1;
		DigitalInput.LightCurtain2OK	:= NOT ButtonPanelSim.LightCurtain2;
		DigitalInput.LightCurtain3OK	:= NOT ButtonPanelSim.LightCurtain3;
		DigitalInput.JogForward			:= (ButtonPanelSim.JogDirection = 0) AND ButtonPanelSim.JogStart;
		DigitalInput.JogReverse			:= (ButtonPanelSim.JogDirection = 1) AND ButtonPanelSim.JogStart;
		DigitalInput.RunMode			:= ButtonPanelSim.ModeSelector = 1;
		DigitalInput.JogMode			:= ButtonPanelSim.ModeSelector = 2;
		// Reset safety circuit
		IF ButtonPanelSim.ResetSafety THEN
			DigitalInput.MachineSafe := TRUE;
		END_IF
		// Trip safety circuit
		IF ButtonPanelSim.EStop OR (gMachine.MachineState <> MAIN_JOG AND
			(ButtonPanelSim.LightCurtain1 OR ButtonPanelSim.LightCurtain2 OR ButtonPanelSim.LightCurtain3)) THEN
			DigitalInput.MachineSafe := FALSE;
			ButtonPanelSim.ResetSafety := FALSE;
		END_IF
	END_IF
	
	// Bypass permissives and IOFault if using sim panel
	IF ButtonPanelSim.PermBypass THEN
		gMachine.Command.PermBypass := TRUE;
		gAlarmInfo.PersistentFlags.IOFault := FALSE;	
	ELSE
		gMachine.Command.PermBypass := FALSE;
	END_IF
	
	// Turn Estop Relay on if PLC is in RUN mode
	gIO.do.EstopRelay := TRUE;
	
	(****** Forcing ******)
	// Enable/Disable forcing
	IF Forcing.btnEnableForcing THEN
		Forcing.visForcing := VIS_SHOW;			// Enable forcing buttons
		gMachine.Command.EnableForcing := TRUE;	// Set gMachine command
		gIO.di := Forcing.ForceDI;				// Overwrite gIO inputs with forced inputs
		DigitalOutput := Forcing.ForceDO;		// Overwrite output channels with forced outputs
	ELSE
		Forcing.visForcing := VIS_HIDE;				// Disable forcing buttons
		gMachine.Command.EnableForcing := FALSE;	// Reset gMachine command
		gIO.di := DigitalInput;						// Write input channels to gIO inputs
		DigitalOutput := gIO.do;					// Write gIO outputs to output channels 
		// Reset all forcing
		brsmemset(ADR(Forcing.ForceDI), 0, SIZEOF(Forcing.ForceDI) );
		brsmemset(ADR(Forcing.ForceDO), 0, SIZEOF(Forcing.ForceDO) );
	END_IF
	
	(****** ABBVFD Digital Inputs ******)
	IF gMachine.MachineType = ACS880 THEN
		gIO.di.MotorOvertemp	:= ABBVFD_DIword.3;	// Safe True
		gIO.di.DBOvertemp		:= ABBVFD_DIword.4;	// Safe True
		gIO.di.MachineSafe		:= NOT ABBVFD_DIword.5;	// Safe False
	END_IF	
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

