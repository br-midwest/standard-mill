
TYPE
	IO_Visibility_enum : 
		(
		VIS_HIDE := 1,
		VIS_SHOW := 0
		);
	Forcing_typ : 	STRUCT 
		ForceDO : do_typ; (*Forced outputs from buttons on I/O Diag page*)
		ForceDI : di_typ; (*Forced inputs from buttons on I/O Diag page*)
		btnEnableForcing : BOOL; (*HMI button to turn forcing on*)
		visForcing : IO_Visibility_enum; (*Make forcing buttons visible/invisible*)
	END_STRUCT;
	ButtonPanelSim_typ : 	STRUCT  (*Inputs from simulated button panel*)
		MillStart : BOOL;
		MillStop : BOOL;
		ModeSelector : USINT;
		JogDirection : USINT;
		JogStart : BOOL;
		EStop : BOOL;
		PermBypass : BOOL;
		LightCurtain1 : BOOL;
		LightCurtain2 : BOOL;
		LightCurtain3 : BOOL;
		ResetSafety : BOOL;
	END_STRUCT;
END_TYPE
