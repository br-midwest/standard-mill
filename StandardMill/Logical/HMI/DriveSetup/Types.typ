
TYPE
	MachineParamUINT_typ : 	STRUCT  (*Parameters with correct units to be used in EPLCommWrite FUB*)
		RatedSpeed : UINT; (*[rpm]*)
		RatedVoltage : UINT; (*[V]*)
		RatedCurrent : UINT; (*[.1 A]*)
		RatedFrequency : UINT; (*[.1 Hz]*)
		RatedPower : UINT; (*[.01 kW]*)
		ProportionalGain : UINT; (*[%]*)
		TimeIntegral : UINT; (*[ms]*)
		FrequencyMax : UINT; (*[.01 kW]*)
		AccelTime : UINT; (*[.1 s]*)
		DecelTime : UINT; (*[.1 s]*)
	END_STRUCT;
	ABBCommParam_typ : 	STRUCT  (*Parameters required for fieldbus control*)
		Ext1Commands : UINT := 12;
		EstopMode : UINT := 2;
		EstopSource : UDINT := 8;
		Ref1Source : UDINT := 4;
		FBAEnable : UINT := 1;
		FBAType : UINT := 136;
		CommProfile : UINT := 0;
		NodeID : UINT := 5;
		T16Scale : UINT := 0;
		ConstantSpeedSel : UDINT := 0;
		RunEnableSource : UDINT := 30;
		RampSetSel : UDINT := 0;
	END_STRUCT;
END_TYPE

(*EPLComm Structures*)

TYPE
	epl_typ : 	STRUCT 
		cmd : eplCmd_typ;
		para : eplPara_typ;
		fub : eplFUB_typ;
		state : state_enum;
	END_STRUCT;
	eplCmd_typ : 	STRUCT 
		readNode : BOOL;
		readThresholdCnt : BOOL;
		writeX2X : BOOL;
	END_STRUCT;
	eplPara_typ : 	STRUCT 
		nodeNr : USINT;
		readData : UDINT;
		writeData : UDINT;
		x2xCycle : UDINT;
	END_STRUCT;
	eplFUB_typ : 	STRUCT 
		EplGetLocalNodeID_0 : EplGetLocalNodeID;
		EplSDOWrite_0 : EplSDOWrite;
		EplSDORead_0 : EplSDORead;
		EplReadX2X : EplSDORead;
	END_STRUCT;
	state_enum : 
		(
		EPL_WAIT,
		EPL_READ_NODE,
		EPL_READ_OE,
		EPL_WRITE_OE,
		EPL_ERROR
		);
END_TYPE
