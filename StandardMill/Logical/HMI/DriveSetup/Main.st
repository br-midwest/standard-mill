
PROGRAM _INIT

END_PROGRAM

PROGRAM _CYCLIC
		
	(****** Motor Configuration ******)
	CASE gMachine.MachineType OF
		
		BRVFD:
			
			(****** Acceleration and Deceleration ******)
			IF AccelDecelChange THEN
				// Update the address of the VFD
				VFDAddress := 'IF1.ST.';		// PLK network is at fixed address
				itoa(NODE_BRVFD, ADR(NodeStr) );	// Convert node number to string
				strcat(ADR(VFDAddress), ADR(NodeStr) );	// Add number to end of address
			
				// Scale parameters properly and convert units
				BRDriveParam.AccelTime		:= REAL_TO_UINT(gMachine.Param.AccelTime * BR_ACCEL_DECEL_SCALE);
				BRDriveParam.DecelTime		:= REAL_TO_UINT(gMachine.Param.DecelTime * BR_ACCEL_DECEL_SCALE);
				
				// Set channels to write to
				ConfigureAccelDecel[0].NodeNumber 		:= NODE_BRVFD;
				ConfigureAccelDecel[0].pDeviceAddress	:= ADR(VFDAddress);
				ConfigureAccelDecel[0].ValueSize 		:= SIZEOF(BRDriveParam.AccelTime);
				ConfigureAccelDecel[0].Index 			:= 16#203C;
				ConfigureAccelDecel[0].SubIndex 		:= 16#2;
				ConfigureAccelDecel[0].pValue 			:= ADR(BRDriveParam.AccelTime);
	
				ConfigureAccelDecel[1].NodeNumber 		:= NODE_BRVFD;
				ConfigureAccelDecel[1].pDeviceAddress 	:= ADR(VFDAddress);
				ConfigureAccelDecel[1].ValueSize 		:= SIZEOF(BRDriveParam.DecelTime);
				ConfigureAccelDecel[1].Index 			:= 16#203C;
				ConfigureAccelDecel[1].SubIndex 		:= 16#3;
				ConfigureAccelDecel[1].pValue 			:= ADR(BRDriveParam.DecelTime);
				
				// Set EPLCommWrite inputs
				EPLCommWrite_1.pMotorPar	:= ADR(ConfigureAccelDecel);
				EPLCommWrite_1.MotorParSize	:= SIZEOF(ConfigureAccelDecel);
				EPLCommWrite_1.ErrorReset	:= gAlarmInfo.EdgeFlags.SendConfigFailed;
			
				// Execute EPLCommWrite and update vis status
				EPLCommWrite_1.Execute 		:= TRUE;
			
				AccelDecelChange := FALSE;
			END_IF
			
			// Reset execute when done
			IF EDGEPOS(EPLCommWrite_1.Done) THEN
				EPLCommWrite_1.Execute := FALSE;
			END_IF
			// Call FUB
			EPLCommWrite_1(); 
			
			// Set errors
			IF EDGEPOS(EPLCommWrite_1.Error) THEN
				ConfigErrID := EPLCommWrite_1.StatusID;
				ConfigErrAddInfo := EPLCommWrite_1.WriteNode.intern.errInfo;
				EPLCommWrite_1.Execute := FALSE;
				gAlarmInfo.EdgeFlags.SendConfigFailed := TRUE;
			END_IF
			
			(****** Motor Configuration ******)
			IF EDGEPOS(SendParameters) THEN
				(****** Retrieve Paramters from gMachine ******)
				// Scale parameters properly and convert units
				BRDriveParam.RatedVoltage		:= REAL_TO_UINT(gMachine.Param.RatedVoltage);
				BRDriveParam.RatedCurrent		:= REAL_TO_UINT(gMachine.Param.RatedCurrent * BR_CURRENT_SCALE);
				BRDriveParam.RatedSpeed			:= REAL_TO_UINT(gMachine.Param.RatedSpeed);
				BRDriveParam.RatedFrequency		:= REAL_TO_UINT(gMachine.Param.RatedFrequency * BR_FREQ_SCALE);
				BRDriveParam.FrequencyMax		:= REAL_TO_UINT(gMachine.Param.FreqencyMax * BR_FREQ_SCALE);
				BRDriveParam.RatedPower			:= REAL_TO_UINT(gMachine.Param.RatedPower * BR_POWER_SCALE);
				BRDriveParam.ProportionalGain	:= REAL_TO_UINT(gMachine.Param.ProportionalGain);
				BRDriveParam.TimeIntegral		:= REAL_TO_UINT(gMachine.Param.TimeIntegral);
	
				(****** B&R PLK Communication ******)
				// Update the address of the VFD
				VFDAddress := 'IF1.ST.';		// PLK network is at fixed address
				itoa(NODE_BRVFD, ADR(NodeStr) );	// Convert node number to string
				strcat(ADR(VFDAddress), ADR(NodeStr) );	// Add number to end of address
	
				// Set channels to write to
				ConfigureVFD[1].NodeNumber := NODE_BRVFD;
				ConfigureVFD[1].pDeviceAddress := ADR(VFDAddress);
				ConfigureVFD[1].ValueSize := SIZEOF(BRDriveParam.RatedVoltage);
				ConfigureVFD[1].Index := 16#2042;
				ConfigureVFD[1].SubIndex := 16#2;
				ConfigureVFD[1].pValue := ADR(BRDriveParam.RatedVoltage);
	
				ConfigureVFD[2].NodeNumber := NODE_BRVFD;
				ConfigureVFD[2].pDeviceAddress := ADR(VFDAddress);
				ConfigureVFD[2].ValueSize := SIZEOF(BRDriveParam.RatedCurrent);
				ConfigureVFD[2].Index := 16#2042;
				ConfigureVFD[2].SubIndex := 16#4;
				ConfigureVFD[2].pValue := ADR(BRDriveParam.RatedCurrent);
	 
				ConfigureVFD[3].NodeNumber := NODE_BRVFD;
				ConfigureVFD[3].pDeviceAddress := ADR(VFDAddress);
				ConfigureVFD[3].ValueSize := SIZEOF(BRDriveParam.RatedFrequency);
				ConfigureVFD[3].Index := 16#2042;
				ConfigureVFD[3].SubIndex := 16#3;
				ConfigureVFD[3].pValue := ADR(BRDriveParam.RatedFrequency);
	 
				ConfigureVFD[4].NodeNumber := NODE_BRVFD;
				ConfigureVFD[4].pDeviceAddress := ADR(VFDAddress);
				ConfigureVFD[4].ValueSize := SIZEOF(BRDriveParam.RatedSpeed);
				ConfigureVFD[4].Index := 16#2042;
				ConfigureVFD[4].SubIndex := 16#5;
				ConfigureVFD[4].pValue := ADR(BRDriveParam.RatedSpeed);
 
				ConfigureVFD[5].NodeNumber := NODE_BRVFD;
				ConfigureVFD[5].pDeviceAddress := ADR(VFDAddress);
				ConfigureVFD[5].ValueSize := SIZEOF(BRDriveParam.FrequencyMax);
				ConfigureVFD[5].Index := 16#2001;
				ConfigureVFD[5].SubIndex := 16#4;
				ConfigureVFD[5].pValue := ADR(BRDriveParam.FrequencyMax);
			
				ConfigureVFD[6].NodeNumber := NODE_BRVFD;
				ConfigureVFD[6].pDeviceAddress := ADR(VFDAddress);
				ConfigureVFD[6].ValueSize := SIZEOF(BRDriveParam.RatedPower);
				ConfigureVFD[6].Index := 16#2042;
				ConfigureVFD[6].SubIndex := 16#E;
				ConfigureVFD[6].pValue := ADR(BRDriveParam.RatedPower);
	 
				ConfigureVFD[7].NodeNumber := NODE_BRVFD;
				ConfigureVFD[7].pDeviceAddress := ADR(VFDAddress);
				ConfigureVFD[7].ValueSize := SIZEOF(BRDriveParam.ProportionalGain);
				ConfigureVFD[7].Index := 16#203D;
				ConfigureVFD[7].SubIndex := 16#4;
				ConfigureVFD[7].pValue := ADR(BRDriveParam.ProportionalGain);
	 
				ConfigureVFD[8].NodeNumber := NODE_BRVFD;
				ConfigureVFD[8].pDeviceAddress := ADR(VFDAddress);
				ConfigureVFD[8].ValueSize := SIZEOF(BRDriveParam.TimeIntegral);
				ConfigureVFD[8].Index := 16#203D;
				ConfigureVFD[8].SubIndex := 16#5;
				ConfigureVFD[8].pValue := ADR(BRDriveParam.TimeIntegral);

				// Set EPLCommWrite inputs
				EPLCommWrite_0.pMotorPar	:= ADR(ConfigureVFD);
				EPLCommWrite_0.MotorParSize	:= SIZEOF(ConfigureVFD);
			
				// Execute EPLCommWrite and update vis status
				EPLCommWrite_0.Execute 		:= TRUE;
				SendParamStatus := 1;
			END_IF
			
			// Tell HMI when EPLCommWrite is done
			IF EDGEPOS(EPLCommWrite_0.Done) THEN
				EPLCommWrite_0.Execute := FALSE;
				SendParamStatus := 2;
			END_IF
			// Reset errors when button is pressed
			EPLCommWrite_0.ErrorReset	:= SendParameters;
			// Call FUB
			EPLCommWrite_0(); 
			
			// Set errors
			IF EDGEPOS(EPLCommWrite_0.Error) THEN
				ConfigErrID := EPLCommWrite_0.StatusID;
				ConfigErrAddInfo := EPLCommWrite_0.WriteNode.intern.errInfo;
				EPLCommWrite_0.Execute := FALSE;
				gAlarmInfo.EdgeFlags.SendConfigFailed := TRUE;
				SendParamStatus := 3;
			END_IF
			// Clear message when configuration is changed
			IF ConfigChange THEN
				SendParamStatus := 0;
				ConfigChange := FALSE;
			END_IF
			
		ACS880:
			// Disable estop if no button panel is present
			IF gMachine.Command.UseBtnSim THEN
				ABBCommParam.EstopSource := 1;
			END_IF
		// Read rated speed from drive
		gMachine.Param.RatedSpeed := DINT_TO_REAL(ABBRatedSpeed);
				
	END_CASE
	
END_PROGRAM

PROGRAM _EXIT


END_PROGRAM

