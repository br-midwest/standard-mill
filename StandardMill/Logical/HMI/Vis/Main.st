
PROGRAM _INIT
	Vis.SplashTimer.PT	:= T#5s;
	Vis.SplashTimer.IN	:= TRUE;
	
	// Initialize GearRatio and RollDiameter to avoid divide by zero
	IF gMachine.Param.GearRatio = 0 THEN
		gMachine.Param.GearRatio	:= 1;
	END_IF
	IF gMachine.Param.RollDiameter = 0 THEN
		gMachine.Param.RollDiameter := 1;
	END_IF
	
	// Retain Set Speed
	Vis.Numeric.SetSpeedPerc := SetSpeedPerc;
	Vis.Numeric.JogSpeedPerc := JogSpeedPerc;
	
END_PROGRAM

PROGRAM _CYCLIC
	
	// Tooltips
	IF Vis.TooltipIndex > 0 THEN
		Vis.Visibility.layerTooltip := VIS_SHOW;
	ELSE
		Vis.Visibility.layerTooltip := VIS_HIDE;
	END_IF
	
	// Time splash screen
	Vis.SplashTimer();
	IF Vis.SplashTimer.Q AND Vis.CurrentPage = 0 AND Vis.CalibrationState <> 4 THEN
		Vis.ChangePage	:= PG_HOME;
	END_IF
	
	// Sound Horn
	gMachine.Command.SoundHorn := Vis.Button.SoundHorn;
	
	// Text on common layer
	Vis.StateText := gMachine.MachineState;
	IF gMachine.MachineState = MAIN_WAIT AND gMachine.Status.Permissives.Motion.MotionPermsOK THEN
		Vis.StateText := 10;
	END_IF
	IF gMachine.MachineState = MAIN_JOG AND gMachine.Status.ActualSpeed = 0 THEN
		Vis.StateText := 11;
	END_IF
	
	(****** Coolant ******)
	// Update coolant mode
	gMachine.CoolantMode := Vis.CoolantMode;
	// Enable or disable coolant start button
	IF Vis.CoolantMode = AUTO THEN
		Vis.Visibility.btnCoolant := VIS_HIDE;
		Vis.Button.CoolantStart := FALSE;
	ELSE
		Vis.Visibility.btnCoolant := VIS_SHOW;
	END_IF
	// Send start request
	gMachine.Command.CoolantStart	:= Vis.Button.CoolantStart;
	
	(****** Numerics ******)
	IF gMachine.Param.GearRatio <> 0 AND gMachine.Param.RollDiameter <> 0 AND gMachine.Param.PullyRatioShaft <> 0 THEN
		Vis.Numeric.LineSpeed		:= gMachine.Status.ActualSpeed *
				gMachine.Param.PullyRatioMotor / gMachine.Param.PullyRatioShaft 
				/ gMachine.Param.GearRatio 
				* gMachine.Param.RollDiameter * 3.14 / 12;
	ELSE
		Vis.Numeric.LineSpeed := 9999;
	END_IF
	Vis.Numeric.MotorCurrent	:= gMachine.Status.MotorCurrent;
	Vis.Numeric.MotorSpeed		:= gMachine.Status.ActualSpeed;
	Vis.Numeric.MotorTorquePerc	:= gMachine.Status.MotorTorquePerc;
	SetSpeedPerc 				:= Vis.Numeric.SetSpeedPerc;	// Retain Set Speed
	JogSpeedPerc 				:= Vis.Numeric.JogSpeedPerc;	// Retain Jog Speed
	IF gIO.di.JogMode THEN
		gMachine.Command.SetSpeed	:= Vis.Numeric.JogSpeedPerc * gMachine.Param.RatedSpeed / PERCENT;
	ELSE
		gMachine.Command.SetSpeed	:= Vis.Numeric.SetSpeedPerc * gMachine.Param.RatedSpeed / PERCENT;
	END_IF
	
	(****** Alarms ******)
	// Check alarm reactions, enable popup, enable buttons, and set color
	IF gAlarmInfo.rxnControlOff THEN
		Vis.Visibility.layerAlarm		:= VIS_SHOW;
		
		Vis.Visibility.btnConfirm		:= VIS_HIDE;
		Vis.Visibility.btnAcknowledge	:= VIS_SHOW;
		Vis.Visibility.btnViewAlarmList	:= VIS_SHOW;
		
		Vis.ColorMap.AlarmBackground 	:= SEV_CONTROL_OFF;
		
	ELSIF gAlarmInfo.rxnStopMachine THEN
		Vis.Visibility.layerAlarm	:= VIS_SHOW;
		
		Vis.Visibility.btnConfirm		:= VIS_HIDE;
		Vis.Visibility.btnAcknowledge	:= VIS_SHOW;
		Vis.Visibility.btnViewAlarmList	:= VIS_SHOW;
		
		Vis.ColorMap.AlarmBackground := SEV_STOP_MACHINE;
		
	ELSIF gAlarmInfo.rxnMessage THEN
		Vis.Visibility.layerAlarm	:= VIS_SHOW;
		
		Vis.Visibility.btnConfirm		:= VIS_SHOW;
		Vis.Visibility.btnAcknowledge	:= VIS_HIDE;
		Vis.Visibility.btnViewAlarmList	:= VIS_HIDE;
		
		Vis.ColorMap.AlarmBackground := SEV_MESSAGE;
		
	ELSE
		Vis.ColorMap.AlarmBackground := SEV_NONE;
		Vis.Visibility.layerAlarm	:= VIS_HIDE;
	END_IF
	
	// Acknowledge alarms and reset tests
	gAlarmList.AcknowledgeAll		:= Vis.Button.AlarmAcknowledge OR Vis.Button.MessageConfirm;
	gMachine.Command.ResetAlarms	:= gAlarmList.AcknowledgeAll;
	IF EDGEPOS(gAlarmList.AcknowledgeAll) THEN
		gAlarmInfo.PersistentFlags.Sev1Test	:= FALSE;
		gAlarmInfo.PersistentFlags.Sev2Test	:= FALSE;
		gAlarmInfo.PersistentFlags.Sev3Test	:= FALSE;
	END_IF
	
	// Highlight list items for AlarmList and AlarmHistory
	FOR node := 0 TO ALARM_LIST_SIZE - 1 DO
		Vis.ColorMap.highlightAlarmList[node] 	:= (INT_TO_UINT(node) = gAlarmList.AlarmList.SelectedIndex);
		Vis.ColorMap.highlightAlarmHistory[node] := (INT_TO_UINT(node) = gAlarmHistory.AlarmList.SelectedIndex);
	END_FOR
	
	(****** Machine-specific Layers ******)
	CASE gMachine.MachineType OF
		SIM:
			Vis.Visibility.layersBR		:= VIS_SHOW;
			Vis.Visibility.layersABB	:= VIS_HIDE;
		BRVFD:
			Vis.Visibility.layersBR		:= VIS_SHOW;
			Vis.Visibility.layersABB	:= VIS_HIDE;
		ACS880:
			Vis.Visibility.layersBR		:= VIS_HIDE;
			Vis.Visibility.layersABB	:= VIS_SHOW;
	END_CASE
	
	// Check for password on config pages
	IF Vis.CurrentPage = 10 OR Vis.CurrentPage = 11 OR Vis.CurrentPage = 12 THEN
		IF Vis.Numeric.AccessLevel = ACCESS_ADMIN THEN
			Vis.Visibility.layerConfigPass := VIS_HIDE;
		END_IF
	ELSE
		// Reset password when leaving config page
		Vis.Numeric.AccessLevel := ACCESS_USER;
		Vis.Visibility.layerConfigPass := VIS_SHOW;
	END_IF
	
	
	// Hide motion permissives if bypassing
	IF gMachine.Command.PermBypass THEN
		Vis.Visibility.txtMotionPerms := VIS_HIDE;
	ELSE
		Vis.Visibility.txtMotionPerms := VIS_SHOW;
	END_IF
	
	// Hide SEND PARAMETERS if not off
	IF gMachine.Status.SwitchedOn THEN
		Vis.Visibility.btnSendParameters := VIS_HIDE;
	ELSE
		Vis.Visibility.btnSendParameters := VIS_SHOW;
	END_IF
	
END_PROGRAM

PROGRAM _EXIT
	 
END_PROGRAM

