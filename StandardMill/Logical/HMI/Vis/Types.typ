
TYPE
	Vis_Visibility_enum : 
		(
		VIS_HIDE := 1,
		VIS_SHOW := 0
		);
	Vis_typ : 	STRUCT 
		CurrentPage : USINT;
		ChangePage : USINT;
		SplashTimer : TON;
		TooltipIndex : UINT; (* Chooses which message to show on tooltip popup*)
		Button : Vis_Button_typ;
		Numeric : Vis_Numeric_typ;
		Visibility : Vis_Visibility_typ;
		ColorMap : Vis_ColorMap_typ;
		CoolantMode : CoolantMode_enum;
		LengthUnit : USINT;
		PowerUnit : USINT;
		CalibrationState : USINT;
		StateText : DINT;
	END_STRUCT;
	Vis_Visibility_typ : 	STRUCT 
		layerTrend : Vis_Visibility_enum;
		layerPerm : Vis_Visibility_enum;
		layerAlarm : Vis_Visibility_enum;
		layerTooltip : Vis_Visibility_enum;
		layersBR : Vis_Visibility_enum;
		layersABB : Vis_Visibility_enum;
		layerConfigPass : Vis_Visibility_enum;
		btnCoolant : Vis_Visibility_enum;
		btnViewAlarmList : Vis_Visibility_enum;
		btnAcknowledge : Vis_Visibility_enum;
		btnConfirm : Vis_Visibility_enum;
		txtMotionPerms : Vis_Visibility_enum;
		btnSendParameters : Vis_Visibility_enum;
	END_STRUCT;
	Vis_Button_typ : 	STRUCT 
		CoolantStart : BOOL;
		CoolantIndependant : BOOL;
		MessageConfirm : BOOL;
		AlarmAcknowledge : BOOL;
		SoundHorn : BOOL;
	END_STRUCT;
	Vis_Numeric_typ : 	STRUCT 
		SetSpeedPerc : REAL := 50;
		JogSpeedPerc : {REDUND_UNREPLICABLE} REAL;
		LineSpeed : REAL;
		MotorSpeed : REAL;
		MotorCurrent : REAL;
		MotorTorquePerc : REAL;
		AccessLevel : AccessLevels_enum;
	END_STRUCT;
	Vis_ColorMap_typ : 	STRUCT 
		highlightAlarmList : ARRAY[0..ALARM_LIST_SIZE]OF BOOL;
		highlightAlarmHistory : ARRAY[0..ALARM_LIST_SIZE]OF BOOL;
		AlarmBackground : Severity_enum;
	END_STRUCT;
	Severity_enum : 
		( (*Alarm severities*)
		SEV_NONE := 0,
		SEV_CONTROL_OFF := 1,
		SEV_STOP_MACHINE := 2,
		SEV_MESSAGE := 3
		);
	AccessLevels_enum : 
		(
		ACCESS_USER := 0,
		ACCESS_ADMIN := 1
		);
END_TYPE
