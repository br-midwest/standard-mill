
PROGRAM _INIT
	 
	MpAlarmXCore_0.Enable	:= TRUE;
	MpAlarmXCore_0.MpLink	:= ADR(gAlarmXCore_CFG);
	
	MpAlarmXHistory_0.Enable	:= TRUE;
	MpAlarmXHistory_0.MpLink	:= ADR(gAlarmXHistory_CFG);
	
	MpAlarmXListUI_0.Enable		:= TRUE;
	MpAlarmXListUI_0.MpLink		:= ADR(gAlarmXCore_CFG);
	MpAlarmXListUI_0.UIConnect	:= ADR(gAlarmList);
	MpAlarmXListUI_0.UISetup.AlarmListSize	:= ALARM_LIST_SIZE;
	
	MpAlarmXHistoryUI_0.Enable		:= TRUE;
	MpAlarmXHistoryUI_0.MpLink		:= ADR(gAlarmXHistory_CFG);
	MpAlarmXHistoryUI_0.UIConnect	:= ADR(gAlarmHistory);
	MpAlarmXHistoryUI_0.UISetup.AlarmListSize	:= ALARM_LIST_SIZE;
	
	TON_BlowerTO.PT := BLOWER_TIMEOUT;
	
END_PROGRAM

PROGRAM _CYCLIC
	
	(* Alarm Code Key:
	10xx: Severity 1 alarms - Control Off
	100x: Safety alarms - Do not require acknowledgement because there is a reset PB
	101x: VFD alarms
	102x: IO alarms
	20xx: Severity 2 alarms - Stop Machine
	30xx: Severity 3 alarms - Messages *)
	
	(****** Set IO alarm flags ******)
	// Light Curtains
	gAlarmInfo.LCNumber := 0;
	IF gMachine.Command.ResetAlarms OR EDGEPOS(gIO.di.MachineSafe) THEN
		// Reset Light Curtain fault
		gAlarmInfo.PersistentFlags.LightCurtainFault := FALSE;
	END_IF
	IF gMachine.MachineState <> MAIN_JOG THEN
		IF NOT gIO.di.LightCurtain1OK AND gMachine.Param.NumLightCurtains >= 1 THEN
			gAlarmInfo.LCNumber := 1;
			gAlarmInfo.PersistentFlags.LightCurtainFault := TRUE;
		ELSIF NOT gIO.di.LightCurtain2OK AND gMachine.Param.NumLightCurtains >= 2 THEN
			gAlarmInfo.LCNumber := 2;
			gAlarmInfo.PersistentFlags.LightCurtainFault := TRUE;
		ELSIF NOT gIO.di.LightCurtain3OK AND gMachine.Param.NumLightCurtains >= 3 THEN
			gAlarmInfo.LCNumber := 3;
			gAlarmInfo.PersistentFlags.LightCurtainFault := TRUE;
		END_IF
	END_IF
	// Other flags
	gAlarmInfo.PersistentFlags.SafetyFault		:= NOT gIO.di.MachineSafe;
	IF NOT gSimulate THEN
		gAlarmInfo.PersistentFlags.CoolantOL 		:= gIO.di.CoolantOL;			// Safe False
		gAlarmInfo.PersistentFlags.BlowerMotorOL	:= gIO.di.BlowerMotorOL;		// Safe False
		gAlarmInfo.PersistentFlags.DBOvertemp		:= NOT gIO.di.DBOvertemp;		// Safe True
		gAlarmInfo.PersistentFlags.MotorOvertemp	:= NOT gIO.di.MotorOvertemp;	// Safe True
		// Blower motor feedback timeout
		TON_BlowerTO.IN	:= gIO.di.MachineSafe;
		IF gIO.di.MachineSafe AND NOT gIO.di.BlowerOn
				AND TON_BlowerTO.Q AND gMachine.MachineType = ACS880 THEN
			gAlarmInfo.PersistentFlags.BlowerFault := TRUE;
		ELSE
			gAlarmInfo.PersistentFlags.BlowerFault := FALSE;
		END_IF
	END_IF
	
	IF MpAlarmXCore_0.Active THEN
		(****** Set Severity 1 Alarms ******)
		// Severity 1 Test Alarm
		IF gAlarmInfo.PersistentFlags.Sev1Test THEN
			MpAlarmXSet(gAlarmXCore_CFG, 'Sev1Test');
		ELSE
			MpAlarmXReset(gAlarmXCore_CFG, 'Sev1Test');
		END_IF
		// 1000 EStopActive
		IF gAlarmInfo.PersistentFlags.SafetyFault THEN
			MpAlarmXSet(gAlarmXCore_CFG, 'SafetyFault');
		ELSE
			MpAlarmXReset(gAlarmXCore_CFG, 'SafetyFault');
		END_IF
		// 1001 LightCurtainFault
		IF gAlarmInfo.PersistentFlags.LightCurtainFault THEN
			IF gAlarmInfo.LCNumber <> 0 THEN
				MpAlarmXSet(gAlarmXCore_CFG, 'LightCurtainFault');
			END_IF
		ELSE
			MpAlarmXReset(gAlarmXCore_CFG, 'LightCurtainFault');
		END_IF
		// 1010 VFDNotFound
		IF gAlarmInfo.PersistentFlags.VFDNotFound THEN
			MpAlarmXSet(gAlarmXCore_CFG, 'VFDNotFound');
		ELSE
			MpAlarmXReset(gAlarmXCore_CFG, 'VFDNotFound');
		END_IF
		// 1011 VFDfault
		IF gAlarmInfo.PersistentFlags.VFDFault THEN
			MpAlarmXSet(gAlarmXCore_CFG, 'VFDFault');
		ELSE
			MpAlarmXReset(gAlarmXCore_CFG, 'VFDFault');
		END_IF
		// 1020 CoolantOL
		IF gAlarmInfo.PersistentFlags.CoolantOL THEN
			MpAlarmXSet(gAlarmXCore_CFG, 'CoolantOL');
		ELSE
			MpAlarmXReset(gAlarmXCore_CFG, 'CoolantOL');
		END_IF
		// 1021 BlowerMotorOL
		IF gAlarmInfo.PersistentFlags.BlowerMotorOL THEN
			MpAlarmXSet(gAlarmXCore_CFG, 'BlowerMotorOL');
		ELSE
			MpAlarmXReset(gAlarmXCore_CFG, 'BlowerMotorOL');
		END_IF
		// 1022 DBOvertemp
		IF gAlarmInfo.PersistentFlags.DBOvertemp THEN
			MpAlarmXSet(gAlarmXCore_CFG, 'DBOvertemp');
		ELSE
			MpAlarmXReset(gAlarmXCore_CFG, 'DBOvertemp');
		END_IF
		// 1023 IOFault
		IF gAlarmInfo.PersistentFlags.IOFault THEN
			MpAlarmXSet(gAlarmXCore_CFG, 'IOFault');
		ELSE
			MpAlarmXReset(gAlarmXCore_CFG, 'IOFault');
		END_IF
		// 1024 CoolantFault
		IF gAlarmInfo.PersistentFlags.CoolantFault THEN
			MpAlarmXSet(gAlarmXCore_CFG, 'CoolantFault');
		ELSE
			MpAlarmXReset(gAlarmXCore_CFG, 'CoolantFault');
		END_IF
		// 1025 MotorOvertemp
		IF gAlarmInfo.PersistentFlags.MotorOvertemp THEN
			MpAlarmXSet(gAlarmXCore_CFG, 'MotorOvertemp');
		ELSE
			MpAlarmXReset(gAlarmXCore_CFG, 'MotorOvertemp');
		END_IF
		// 1026 BlowerFault
		IF gAlarmInfo.PersistentFlags.BlowerFault THEN
			MpAlarmXSet(gAlarmXCore_CFG, 'BlowerFault');
		ELSE
			MpAlarmXReset(gAlarmXCore_CFG, 'BlowerFault');
		END_IF
	
		(****** Set Severity 2 Alarms ******)
		// Severity 2 Test Alarm
		IF gAlarmInfo.PersistentFlags.Sev2Test THEN
			MpAlarmXSet(gAlarmXCore_CFG, 'Sev2Test');
		ELSE
			MpAlarmXReset(gAlarmXCore_CFG, 'Sev2Test');
		END_IF
		// 2000 ModeChangeWhileRunning
		IF gAlarmInfo.EdgeFlags.ModeChangeWhileRunning THEN
			MpAlarmXSet(gAlarmXCore_CFG, 'ModeChangeWhileRunning');
			gAlarmInfo.EdgeFlags.ModeChangeWhileRunning := FALSE;
		END_IF
		// 2001 SendConfigFailed
		IF gAlarmInfo.EdgeFlags.SendConfigFailed THEN
			MpAlarmXSet(gAlarmXCore_CFG, 'SendConfigFailed');
			gAlarmInfo.EdgeFlags.SendConfigFailed := FALSE;
		END_IF
		// 2002 CoolantModeChanged
		IF gAlarmInfo.EdgeFlags.CoolantModeChanged THEN
			MpAlarmXSet(gAlarmXCore_CFG, 'CoolantModeChanged');
			gAlarmInfo.EdgeFlags.CoolantModeChanged := FALSE;
		END_IF

	
		(****** Set Severity 3 Alarms ******)
		// Severity 3 Test Alarm
		IF gAlarmInfo.PersistentFlags.Sev3Test THEN
			MpAlarmXSet(gAlarmXCore_CFG, 'Sev3Test');
		ELSE
			MpAlarmXReset(gAlarmXCore_CFG, 'Sev3Test');
		END_IF
		// 3000 JogNotAllowed
		IF gAlarmInfo.EdgeFlags.JogNotAllowed THEN
			MpAlarmXSet(gAlarmXCore_CFG, 'JogNotAllowed');
			gAlarmInfo.EdgeFlags.JogNotAllowed := FALSE;
		END_IF
		// 3001 StartNotAllowed
		IF gAlarmInfo.EdgeFlags.StartNotAllowed THEN
			MpAlarmXSet(gAlarmXCore_CFG, 'StartNotAllowed');
			gAlarmInfo.EdgeFlags.StartNotAllowed := FALSE;
		END_IF
		// 3002 CoolantRequired
		IF gAlarmInfo.EdgeFlags.CoolantRequired THEN
			MpAlarmXSet(gAlarmXCore_CFG, 'CoolantRequired');
			gAlarmInfo.EdgeFlags.CoolantRequired := FALSE;
		END_IF
		// 3003 MotionPermsReqiured
		IF gAlarmInfo.EdgeFlags.MotionPermsReqiured THEN
			MpAlarmXSet(gAlarmXCore_CFG, 'MotionPermsReqiured');
			gAlarmInfo.EdgeFlags.MotionPermsReqiured := FALSE;
		END_IF
		// 3004 NoMachineConfig
		IF gAlarmInfo.EdgeFlags.NoMachineConfig THEN
			MpAlarmXSet(gAlarmXCore_CFG, 'NoMachineConfig');
			gAlarmInfo.EdgeFlags.NoMachineConfig := FALSE;
		END_IF
	END_IF
	
	(****** Update AlarmInfo ******)
	gAlarmInfo.ActiveAlarms		:= MpAlarmXCore_0.ActiveAlarms;
	gAlarmInfo.PendingAlarms	:= MpAlarmXCore_0.PendingAlarms;
	gAlarmInfo.rxnControlOff	:= MpAlarmXCheckReaction(gAlarmXCore_CFG, 'CONTROL_OFF');
	gAlarmInfo.rxnMessage		:= MpAlarmXCheckReaction(gAlarmXCore_CFG, 'MESSAGE');
	gAlarmInfo.rxnStopMachine	:= MpAlarmXCheckReaction(gAlarmXCore_CFG, 'STOP_MACHINE');
	
	(****** Call Function Blocks ******)
	MpAlarmXCore_0();
	MpAlarmXHistory_0();
	MpAlarmXListUI_0();
	MpAlarmXHistoryUI_0();
	TON_BlowerTO();
	
END_PROGRAM

PROGRAM _EXIT
	
	MpAlarmXCore_0(		Enable := FALSE);
	MpAlarmXHistory_0(	Enable := FALSE);
	MpAlarmXListUI_0(	Enable := FALSE);
	MpAlarmXHistoryUI_0(Enable := FALSE);
	
END_PROGRAM

