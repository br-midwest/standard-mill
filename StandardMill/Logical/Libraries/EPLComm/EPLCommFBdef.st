(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: VfdConfigFBdef
 * File: VfdConfigFBdef.st
 * Author: filsonc
 * Created: January 3, 2018
 ********************************************************************
 * Implementation OF Function Block VfdConfigRead
 * Description:
 * - Reads VFD parameters from a P74/P84 ACOPOSinverter over POWERLINK 
     during runtime
   - Uses small state machine to "loop" through the parameter input list
   - Uses AsEPL library for POWERLINK communication with Inverter
     communication list
   - Uses AsBrStr library to copy parameter structure into AsEPL library 
 ********************************************************************)

(* Function block to write list of VFD parameters from a P74/P84 ACOPOSinverter *)
FUNCTION_BLOCK EPLCommWrite
	(* Reset Powerlink Device parameters on new execution *)
	IF EDGEPOS(Execute) THEN
		WriteNode.enable	:= TRUE;
		Done				:= FALSE;
		Busy				:= TRUE;
		node 					:= 0;
	END_IF

	IF (Execute) THEN
		(* Function block state machine to loop through input parameter list *)
		CASE State OF
			(* IDLE state until parameter is successfully written to the drive or there is an error *)
			0:	
				(* When parameter is succcessfully written to the drive, go to evaluate next parameter in the list *)
				IF (WriteNode.status <> ERR_FUB_BUSY) THEN
					IF (WriteNode.status = 0) THEN
						WriteNode.enable	:= FALSE;
						State				:= 1;
						
					(* Reset the write function but stay on current parameter *)	
					ELSIF (WriteNode.errorinfo = EPL_ERR_SDO_TIMEOUT) THEN
						WriteNode.enable	:= FALSE;
						
					(* Otherwise, go to error reporting state *)	
					ELSIF (WriteNode.status <> 65534) THEN
						WriteNode.enable	:= FALSE;
						Error				:= TRUE;
						StatusID			:= WriteNode.status;
					
						State				:= 100;
						
					ELSE
						WriteNode.enable	:= TRUE;
					END_IF
				END_IF
		
			(* Parameter list is checked if all parameters have been written to the VFD *)
			1:
				(* Go to the next parameter in the list *)
				node	:= node + 1;
				
				IF ((node * SIZEOF(VfdConfig) + 1) >= MotorParSize) THEN
					node 				:= 0;
					Done			:= TRUE;
					Busy			:= FALSE;
				ELSE
					WriteNode.enable	:= TRUE;
				END_IF
					
				State	:= 0;
		
			(* Error handling state *)		
			100:
				Error	:= TRUE;
		
				(* Reset function block on error reset command *)
				IF ErrorReset THEN
					ErrorReset			:= FALSE;
					Error				:= FALSE;
					Done				:= FALSE;
					Busy				:= FALSE;
					WriteNode.enable	:= FALSE;
					State				:= 0;
				END_IF
		
		END_CASE
		(* END OF STATE MACHINE *)
		
		(* Memory management - Copying Index, Subindex, and Value memory in to AsEPL Read function block parameters *)
		IF EDGEPOS(WriteNode.enable) THEN
			brsmemcpy(ADR(WriteNode.index), 	(pMotorPar + node*SIZEOF(VfdConfig)), SIZEOF(VfdConfig.Index));
			brsmemcpy(ADR(WriteNode.subindex), 	(pMotorPar + node*SIZEOF(VfdConfig) + SIZEOF(VfdConfig.Index)), SIZEOF(VfdConfig.SubIndex));
			brsmemcpy(ADR(WriteNode.node), 		(pMotorPar + node*SIZEOF(VfdConfig) + SIZEOF(VfdConfig.Index) + SIZEOF(VfdConfig.SubIndex)), SIZEOF(VfdConfig.NodeNumber));
			brsmemcpy(ADR(WriteNode.pData), 	(pMotorPar + node*SIZEOF(VfdConfig) + SIZEOF(VfdConfig.Index) + SIZEOF(VfdConfig.SubIndex) + SIZEOF(VfdConfig.NodeNumber)), SIZEOF(VfdConfig.pValue));
			brsmemcpy(ADR(WriteNode.pDevice), 	(pMotorPar + node*SIZEOF(VfdConfig) + SIZEOF(VfdConfig.Index) + SIZEOF(VfdConfig.SubIndex) + SIZEOF(VfdConfig.NodeNumber) + SIZEOF(VfdConfig.pValue)), SIZEOF(VfdConfig.pDeviceAddress));
			brsmemcpy(ADR(WriteNode.datalen),	(pMotorPar + node*SIZEOF(VfdConfig) + SIZEOF(VfdConfig.Index) + SIZEOF(VfdConfig.SubIndex) + SIZEOF(VfdConfig.NodeNumber) + SIZEOF(VfdConfig.pValue) + SIZEOF(VfdConfig.pDeviceAddress)), SIZEOF(VfdConfig.ValueSize));
		END_IF
	
		(* Function block calls *)
		WriteNode();

	ELSE
		WriteNode.enable	:= FALSE;
		WriteNode();
	END_IF
END_FUNCTION_BLOCK

(* Function block to read list of VFD parameters from a P74/P84 ACOPOSinverter *)
FUNCTION_BLOCK EPLCommRead
	(* Reset Powerlink Device parameters on new execution *)
	IF EDGEPOS(Execute) THEN
		ReadNode.enable		:= TRUE;
		Done				:= FALSE;
		Busy				:= TRUE;
		node 					:= 0;
	END_IF

	IF (Execute) THEN
		(* Function block state machine to loop through input parameter list *)
		CASE State OF
			(* IDLE state until parameter is successfully written to the drive or there is an error *)
			0:	
				(* When parameter is succcessfully written to the drive, go to evaluate next parameter in the list *)
				IF (ReadNode.status <> ERR_FUB_BUSY) THEN
					IF (ReadNode.status = 0) THEN
						ReadNode.enable		:= FALSE;
						State				:= 1;
						
					(* Reset the Read function and wait if there is an SDO timeout *)
					ELSIF (ReadNode.errorinfo = EPL_ERR_SDO_TIMEOUT) THEN
						ReadNode.enable		:= FALSE;
						
					(* Otherwise, go to error reporting state *)
					ELSIF (ReadNode.status <> 65534) THEN
						ReadNode.enable		:= FALSE;
						Error				:= TRUE;
						StatusID			:= ReadNode.status;
					
						State				:= 100;
					ELSE
						ReadNode.enable		:= TRUE;
					END_IF
				END_IF
		
			(* Parameter list is checked if all parameters have been written to the VFD *)
			1:
				(* Go to the next parameter in the list *)
				node	:= node + 1;
				
				IF ((node * SIZEOF(VfdConfig) + 1) >= MotorParSize) THEN
					node 				:= 0;
					Done			:= TRUE;
					Busy			:= FALSE;
					
				ELSE
					ReadNode.enable	:= TRUE;
				END_IF
					
				State	:= 0;
		
				(* Error handling state *)		
			100:
				Error	:= TRUE;
		
				State	:= 1;
				
				(* Reset function block on error reset command *)
				IF ErrorReset THEN
					ErrorReset			:= FALSE;
					Error				:= FALSE;
					Done				:= FALSE;
					Busy				:= FALSE;
					ReadNode.enable		:= FALSE;
					State				:= 0;
				END_IF
		
		END_CASE
		(* END OF STATE MACHINE *)
		
		(* Memory management - Copying Index, Subindex, and Value memory in to AsEPL Read function block parameters *)
		IF (ReadNode.enable) THEN
			brsmemcpy(ADR(ReadNode.index), 		(pMotorPar + node*SIZEOF(VfdConfig)), SIZEOF(VfdConfig.Index));
			brsmemcpy(ADR(ReadNode.subindex), 	(pMotorPar + node*SIZEOF(VfdConfig) + SIZEOF(VfdConfig.Index)), SIZEOF(VfdConfig.SubIndex));
			brsmemcpy(ADR(ReadNode.node), 		(pMotorPar + node*SIZEOF(VfdConfig) + SIZEOF(VfdConfig.Index) + SIZEOF(VfdConfig.SubIndex)), SIZEOF(VfdConfig.NodeNumber));
			brsmemcpy(ADR(ReadNode.pData), 		(pMotorPar + node*SIZEOF(VfdConfig) + SIZEOF(VfdConfig.Index) + SIZEOF(VfdConfig.SubIndex) + SIZEOF(VfdConfig.NodeNumber)), SIZEOF(VfdConfig.pValue));
			brsmemcpy(ADR(ReadNode.pDevice), 	(pMotorPar + node*SIZEOF(VfdConfig) + SIZEOF(VfdConfig.Index) + SIZEOF(VfdConfig.SubIndex) + SIZEOF(VfdConfig.NodeNumber) + SIZEOF(VfdConfig.pValue)), SIZEOF(VfdConfig.pDeviceAddress));
			brsmemcpy(ADR(ReadNode.datalen),	(pMotorPar + node*SIZEOF(VfdConfig) + SIZEOF(VfdConfig.Index) + SIZEOF(VfdConfig.SubIndex) + SIZEOF(VfdConfig.NodeNumber) + SIZEOF(VfdConfig.pValue) + SIZEOF(VfdConfig.pDeviceAddress)), SIZEOF(VfdConfig.ValueSize));
		END_IF
	
		(* Function block calls *)
		ReadNode();

	ELSE
		ReadNode.enable	:= FALSE;
		ReadNode();
	END_IF
	
END_FUNCTION_BLOCK

(* Function block to determine if the VFD has autotuned properly *)
FUNCTION_BLOCK VfdAutotuneStatus
	// Reset communication parameters
	IF EDGEPOS(Execute) THEN
		ReadNode.node		:= NodeNumber;
		ReadNode.pDevice	:= pDeviceAddress;
		ReadNode.index		:= 16#2042;
		ReadNode.subindex	:= 16#a;
		ReadNode.datalen	:= SIZEOF(WORD);
		ReadNode.pData		:= ADR(AutotuneStatus);
		
		Busy				:= TRUE;
		Done				:= FALSE;
		Error				:= FALSE;
		StatusID			:= 0;
	END_IF
	
	IF (Execute) THEN
		CASE AutotuneStatus OF
			(* Case for if the autotune completes successfully *)
			vfdTUNE_DONE:
				ReadNode.enable	:= FALSE;
			
				Done	:= TRUE;
				Busy	:= FALSE;
				Execute	:= FALSE;
				
			vfdTUNE_FAILED:
				ReadNode.enable	:= FALSE;
				Done	:= TRUE;
				Busy	:= FALSE;
				Execute	:= FALSE;
				
			(* If the autotune is completing *)
			ELSE
				// If the function block is not busy, handle communication				
				IF (ReadNode.status <> ERR_FUB_BUSY) THEN
					// If the node has been read, disable the function block
					IF (ReadNode.status = 0) THEN
						ReadNode.enable	:= FALSE;
					// Otherwise, tell it to read the next node
					ELSIF (ReadNode.status = 65534) THEN
						ReadNode.enable	:= TRUE;
					END_IF
				END_IF			
		END_CASE
	ELSE
		ReadNode.enable	:= FALSE;
	END_IF
	
	(* Function block calls *)
	ReadNode();
	
END_FUNCTION_BLOCK