(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: VfdConfig
 * File: VfdConfig.typ
 * Author: filsonc
 * Created: December 17, 2017
 ********************************************************************
 * Implementation of TYPE file VfdConfig
 * Description:
 * - Data structures necessary for using function blocks in VfdConfig
     Library
 ********************************************************************)

TYPE
	EPLCommParType : 	STRUCT  (*Motor parameter configuration structure*)
		Index : UINT; (*P74/84 Communication parameters index*)
		SubIndex : USINT; (*P74/84 Communication parameters subindex*)
		NodeNumber : USINT; (*Node number of the P74/84*)
		pValue : UDINT; (*Parameter value as a unsigned 16 bit integer*)
		pDeviceAddress : UDINT; (*Device EPL address as a pointer to a string*)
		ValueSize : UDINT; (*Size of the target value*)
	END_STRUCT;
	VfdAutotuneStatusEnum : 
		(
		vfdTUNE_NOT_DONE := 0,
		vfdTUNE_PENDING := 1,
		vfdTUNE_IN_PROGRESS := 2,
		vfdTUNE_FAILED := 3,
		vfdTUNE_DONE := 4,
		vfdTUNE_STRD := 5,
		vfdTUNE_CUSTOM := 6
		);
END_TYPE
