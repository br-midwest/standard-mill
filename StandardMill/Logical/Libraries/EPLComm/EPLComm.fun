(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: VfdConfig
 * File: VfdConfig.fun
 * Author: filsonc
 * Created: December 17, 2017 
 ********************************************************************
 * Implementation of VfdConfig Function Blocks
 * 1) VfdConfigSet - Writes a set of configuration parameters
	  to a drive connected via POWERLINK at runtime
 ********************************************************************)

FUNCTION_BLOCK EPLCommRead
	VAR_INPUT
		Execute : BOOL;
		pMotorPar : {REDUND_UNREPLICABLE} UDINT;
		MotorParSize : UDINT;
		ErrorReset : BOOL;
	END_VAR
	VAR_OUTPUT
		Busy : BOOL;
		Error : BOOL;
		StatusID : DINT;
		Done : BOOL;
	END_VAR
	VAR
		State : USINT;
		ReadNode : EplSDORead;
		node : USINT;
		zzEdge00000 : BOOL;
		VfdConfig : EPLCommParType;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK EPLCommWrite
	VAR_INPUT
		Execute : BOOL;
		pMotorPar : {REDUND_UNREPLICABLE} UDINT;
		MotorParSize : UDINT;
		ErrorReset : BOOL;
	END_VAR
	VAR_OUTPUT
		Busy : BOOL;
		Error : BOOL;
		StatusID : DINT;
		Done : BOOL;
	END_VAR
	VAR
		State : USINT;
		WriteNode : EplSDOWrite;
		zzEdge00000 : BOOL;
		node : USINT;
		VfdConfig : EPLCommParType;
		zzEdge00001 : BOOL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK VfdAutotuneStatus
	VAR_INPUT
		Execute : BOOL;
		NodeNumber : USINT;
		pDeviceAddress : UDINT;
	END_VAR
	VAR_OUTPUT
		AutotuneStatus : VfdAutotuneStatusEnum;
		Busy : BOOL;
		Done : BOOL;
		Error : BOOL;
		StatusID : UDINT;
	END_VAR
	VAR
		ReadNode : EplSDORead;
		zzEdge00000 : BOOL;
	END_VAR
END_FUNCTION_BLOCK
