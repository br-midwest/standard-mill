
(* UDFB to convert Feet per minute as entered by an operator to RPM which will be sent to the VFD *)
FUNCTION_BLOCK FPMtoRPM
	ScaleFactor := BaseRPM / BaseFPM;
	RPM := REAL_TO_INT((FPM + Offset) * ScaleFactor);
			
END_FUNCTION_BLOCK
