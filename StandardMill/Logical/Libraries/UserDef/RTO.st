(* UDFB: Switch-on delay with time value retained *)
FUNCTION_BLOCK RTO
	CurrentTime := clock_ms();
	IF EDGEPOS(IN) THEN
		StartTime := CurrentTime;
	END_IF
	IF IN THEN
		ET := CurrentTime - StartTime + PreviousET;
	END_IF
	IF EDGENEG(IN) THEN
		PreviousET := ET;
	END_IF
	IF RESET THEN
		StartTime := CurrentTime;
		PreviousET := 0;
		ET := 0;
	END_IF
	IF ET >= PT THEN
		Q := 1;
	ELSE
		Q := 0;
	END_IF		
END_FUNCTION_BLOCK
