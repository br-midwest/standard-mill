
FUNCTION_BLOCK FPMtoRPM (*RETURN INT RPM*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		FPM : INT;
		Offset : INT;
	END_VAR
	VAR_OUTPUT
		RPM : INT;
	END_VAR
	VAR
		BaseRPM : INT := 1200;
		BaseFPM : INT := 100;
		ScaleFactor : REAL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK RTO (*UDFB: Switch-on delay with time value retained*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		IN : BOOL;
		RESET : BOOL;
		PT : TIME;
	END_VAR
	VAR_OUTPUT
		Q : BOOL;
		ET : TIME;
	END_VAR
	VAR
		CurrentTime : TIME;
		StartTime : TIME;
		PreviousET : TIME;
		zzEdge00000 : BOOL;
		zzEdge00001 : BOOL;
	END_VAR
END_FUNCTION_BLOCK
