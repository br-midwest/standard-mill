
{REDUND_ERROR} FUNCTION_BLOCK DS402Basic (*A function block created to easy and intuitive ACOPOSinverter movement control*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Enable : {REDUND_UNREPLICABLE} BOOL; (*The function block is active as long as this input is set *) (* *) (*#PAR#*)
		ErrorAcknowledge : {REDUND_UNREPLICABLE} BOOL; (*Use to error acknowledge *) (* *) (*#PAR#*)
		Velocity : {REDUND_UNREPLICABLE} INT; (*Velocity [rpm] or [Hz] *) (* *) (*#PAR#*)
		Direction : BOOL; (*Direction of movement *) (* *) (*#PAR#*)
		Update : BOOL; (*Update the parameters - edge sensitive *) (* *) (*#PAR#*)
		StatusWord : {REDUND_UNREPLICABLE} UINT; (*ACPOPOSinverter status(required I / O mapping) *) (* *) (*#CYC#*)
		ModuleOk : {REDUND_UNREPLICABLE} BOOL; (*ACPOPOSinverter status(required I / O mapping) *) (* *) (*#CYC#*)
		SwitchOn : BOOL; (*Switchtch on the voltage- edge sensitive*) (* *) (* #CMD#*)
		MoveVelocity : {REDUND_UNREPLICABLE} BOOL; (*Start a movement with defined speed and acceleration- edge sesitive *) (* *) (*#CMD#*)
		Stop : {REDUND_UNREPLICABLE} BOOL; (*Quick stop command- edge sensitive *) (* *) (*#CMD#*)
		Simulate : BOOL; (*Ignore the real StatusWord and insted assume that commands are being executed*)
	END_VAR
	VAR_OUTPUT
		Active : {REDUND_UNREPLICABLE} BOOL; (*Indicates whether the function block is active *) (* *) (*#PAR#*)
		Error : {REDUND_UNREPLICABLE} BOOL; (*Indicates that the drive is in error state *) (* *) (*#PAR#*)
		StatusID : {REDUND_UNREPLICABLE} DS402BasicStateEnum; (*Current state*) (* *) (*#PAR#*)
		SetDirection : BOOL; (*Current direction  *) (* *) (*#PAR#*)
		SetVelocity : INT; (*Current set point (velocity) *) (* *) (*#PAR#*)
		SwitchedOn : BOOL; (*Indicates whether the drive is switched on *) (* *) (*#CMD#*)
		ReadyToSwitchOn : {REDUND_UNREPLICABLE} BOOL; (*Indicates whether the drive is ready to switched on *) (* *) (*#CMD#*)
		InMotion : {REDUND_UNREPLICABLE} BOOL; (*Indicates whether the drive is moving *) (* *) (*#CMD#*)
		SetVelocityReached : BOOL; (*Indicates whether the drive velocity reachd the set point *) (* *) (*#CMD#*)
		ControlWord : {REDUND_UNREPLICABLE} UINT; (*Control word - command sent to ACOPOSinverter *) (* *) (*#CMD#*)
	END_VAR
	VAR
		State : DS402BasicStateEnum;
		ControlOUT : DS402BasicControlOutputType;
		ControlIN : DS402BasicControlnputType;
		ErrorSim : BOOL; (*Simulate an error*)
	END_VAR
END_FUNCTION_BLOCK
