#define TRUE 1
#define FALSE 0
#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif
#include "DS402.h"
#ifdef __cplusplus
	};
#endif

void DS402Basic(struct DS402Basic* inst)
{	
	/*************************** Simulation Logic - bolson Oct22 ***************************/
	// "x<<y" 		bit shifts x by y bits
	// "|= 1<<x"	sets bit x to 1
	// "&= ~(1<<x)"	resets bit x to 0
	// "&1<<x"		checks the value of bit x
	if(inst->Simulate == TRUE)
	{
		// Drive is ready to switch on
		inst->ModuleOk = TRUE;
		inst->StatusWord |= 1<<0;	// SW.0=1
		inst->StatusWord |= 1<<5;	// SW.5=1
		// Switch on command
		if( (inst->ControlWord & 1<<0) > 0){	// CW.0 == 1
			inst->StatusWord |= 1<<1;	// SW.1 = 1
		}else{
			inst->StatusWord &= ~(1<<1);	// SW.1 = 0
		}
		// Move command
		if( (inst->ControlWord & 1<<3) > 0){	// CW.3 == 1
			inst->StatusWord |= 1<<2;	// SW.2 = 1
			// Quick stop
			if( (inst->ControlWord & 1<<2) == 0){	// CW.2 == 0
				inst->StatusWord &= ~(1<<5);	// SW.5=0
			}
		}else{
			inst->StatusWord &= ~(1<<2);	// SW.2 = 0
		}
		// Error
		if(inst->ErrorSim==TRUE){
			inst->StatusWord |= 1<<3;	// SW.3 = 1
			inst->StatusWord &= ~7;		// SW.0,1,2 = 0
		}
		// Acknowledge Error
		if(inst->ErrorAcknowledge==TRUE){
			inst->ErrorSim = FALSE;
		}
	}
	
	/***************************************************************INPUT HANDLING PART*******************************************************************/
	/**********In the logic part of a FB, it are not used directly inputs/outputs but variables located in the ControlIN / inst->ControlOUT structures**********/
	/****************************************************Assign inputs of FB to ControlIN structure*******************************************************/
	inst->ControlIN.Command.MoveVelocity			=inst->MoveVelocity;
	inst->ControlIN.Command.Stop					=inst->Stop;
	inst->ControlIN.Command.SwitchOn				=inst->SwitchOn;
	inst->ControlIN.Command.ErrorAcknowledge		=inst->ErrorAcknowledge;
	inst->ControlIN.Command.Enable					=inst->Enable;
	inst->ControlIN.Status.ModuleOk					=inst->ModuleOk;
	inst->ControlIN.Status.StatusWord				=inst->StatusWord;
	inst->ControlIN.Command.Update					=inst->Update;
	inst->ControlIN.Parameters.Direction 			=inst->Direction;
	inst->ControlIN.Parameters.Velocity 			=inst->Velocity;
	/********If is "MoveVelocity" or "Update" command, assigning ControlIN parameters to inst->ControlOUT parameters (only parameters that need an update)*************/
	if ((inst->ControlIN.Command.Update > inst->ControlIN.Command.UpdateOld) || (inst->ControlIN.Command.MoveVelocity > inst->ControlIN.Command.MoveVelocityOld  ))
	{
		inst->ControlOUT.Status.SetVelocity		=inst->ControlIN.Parameters.Velocity;
		inst->ControlOUT.Status.SetDirection		=inst->ControlIN.Parameters.Direction;
	}
	/***************************************************************STATE MACHINE PART*******************************************************************/
	/**********for more details regarding state machine(states number, command numbers),please check the DS 402 library documentation********************/
	switch (inst->State)
	{
		case	ds402BASIC_ERROR:		
			inst->ControlOUT.Status.Error						=TRUE;
			/*********************To acknowledge the error, the rising edge is required (fault acknowledge command in CiA 402- 128)**************************/
			if(inst->ControlIN.Command.ErrorAcknowledge>inst->ControlIN.Command.ErrorAcknowledgeOld)
			{
			inst->ControlOUT.Status.ControlWord			= ds402BASIC_CMD_ERROR_ACK;	
			}
			else
			{
			inst->ControlOUT.Status.ControlWord			= ds402BASIC_CMD_DISABLE;	
			}
			/******************if the error disappears then the state changes depending on the state of drive********************************/
			if (((inst->StatusWord | ds402BASIC_BIT_MASK_TWO) == ds402BASIC_SWITCHED_ON_STATUS) && inst->ControlIN.Status.ModuleOk) 
			{
				inst->ControlOUT.Status.Error					=FALSE;
				inst->State									=ds402BASIC_SWITCHED_ON;
			}
			if (((inst->StatusWord | ds402BASIC_BIT_MASK_TWO) == ds402BASIC_READY_SWITCH_STATUS) && inst->ControlIN.Status.ModuleOk)
			{
				inst->ControlOUT.Status.Error					=FALSE;
				inst->State									=ds402BASIC_READY_TO_SWITCH_ON;
			}
			if (((inst->StatusWord | ds402BASIC_BIT_MASK_ONE) == ds402BASIC_WAIT_HW_STATUS)  && inst->ControlIN.Status.ModuleOk)
			{
				inst->ControlOUT.Status.Error					=FALSE;
				inst->State									=ds402BASIC_WAIT_HW;
			}	
			break;
		
		case ds402BASIC_WAIT_HW:
			/***********************************The "Active" output is TRUE only if the FB is enabled and ready to switch on**********************************/
			inst->ControlOUT.Status.Active					=FALSE;	
			/**************Check if the drive is in the "SWITCH_ON_DISABLED" state, then command ds402BASIC_CMD_ENABLE_VOLTAGE(6) can be used******************/	
			if (((inst->StatusWord | ds402BASIC_BIT_MASK_ONE) == ds402BASIC_WAIT_HW_STATUS) && inst->ControlIN.Command.Enable) 
			{
				inst->ControlOUT.Status.ControlWord			= ds402BASIC_CMD_ENABLE_VOLTAGE;	
			}
			/****************Check if after command ds402BASIC_CMD_ENABLE_VOLTAGE(6),drive changed to "READY_TO_SWITCH_ON" state ******************************/
			if ( ((inst->StatusWord | ds402BASIC_BIT_MASK_TWO) == ds402BASIC_READY_SWITCH_STATUS) && inst->ControlIN.Command.Enable && inst->ControlIN.Status.ModuleOk )
			{
				inst->State									=ds402BASIC_READY_TO_SWITCH_ON;
			}
			break;
			
		case ds402BASIC_READY_TO_SWITCH_ON:
			/***************In this state, the drive is ready for operation and FB is enabled - "Active"(FB output) flag is set to TRUE.**********************/	
			inst->ControlOUT.Status.Active					=TRUE;
			/***************If the user wants to disable the FB, a negative edge is required (FB "EnableE input) then the "Disable" command is given**********/	
			if(	inst->ControlIN.Command.Enable<inst->ControlIN.Command.EnableOld )
			{
				inst->ControlOUT.Status.ControlWord			= ds402BASIC_CMD_DISABLE;	
			}
			/*****************Check if after command ds402BASIC_CMD_DISABLE(0),drive changed to "ds402BASIC_WAIT_HW_STATUS" state ******************************/
			if (inst->ControlIN.Command.Enable==0 && (inst->StatusWord | ds402BASIC_BIT_MASK_ONE)	== ds402BASIC_WAIT_HW_STATUS)
			{
				inst->State									= ds402BASIC_WAIT_HW;		
			}
			/**********************To switch on, the rising egde is required, then ds402BASIC_CMD_SWITCH_ON(7) command is given*********************************/
			if(inst->ControlIN.Command.SwitchOn > inst->ControlIN.Command.SwitchOnOld )
			{
				inst->ControlOUT.Status.ControlWord			= ds402BASIC_CMD_SWITCH_ON;	
			}
			/***************Check if after command ds402BASIC_CMD_SWITCH_ON(7),drive changed to "ds402BASIC_SWITCHED_ON_STATUS" state ***************************/
			if ((inst->StatusWord | ds402BASIC_BIT_MASK_TWO) == ds402BASIC_SWITCHED_ON_STATUS)
			{
				inst->State									= ds402BASIC_SWITCHED_ON;
			}
			break;
		
		case ds402BASIC_SWITCHED_ON:
			/***************If the user wants to disable the FB, a negative edge is required (FB "EnableE input) then the "Disable" command is given**********/	
			if(	inst->ControlIN.Command.Enable<inst->ControlIN.Command.EnableOld)
			{
				inst->ControlOUT.Status.ControlWord			= ds402BASIC_CMD_DISABLE;	
			}
			/****************Check if after command Enable =FALSE,drive changed to ds402BASIC_WAIT_HW_STATUS state-and then a change of state *******************/	
			if (inst->ControlIN.Command.Enable==0 && (inst->StatusWord | ds402BASIC_BIT_MASK_ONE)	== ds402BASIC_WAIT_HW_STATUS)
			{
				inst->State									=ds402BASIC_WAIT_HW;
			}
			/**********************To switch off, the negative egde is required, then ds402BASIC_CMD_SWITCH_ON(7) command is given*********************************/
			if(inst->ControlIN.Command.SwitchOn < inst->ControlIN.Command.SwitchOnOld)
			{
				inst->ControlOUT.Status.ControlWord			= ds402BASIC_CMD_ENABLE_VOLTAGE;
			}
			/******Check if after command ds402BASIC_CMD_ENABLE_VOLTAGE(6),drive changed to ds402BASIC_READY_TO_SWITCH_ON state-and then a change of state *******/
			if ( (inst->StatusWord | ds402BASIC_BIT_MASK_TWO) == ds402BASIC_READY_SWITCH_STATUS)
			{
				inst->State									=ds402BASIC_READY_TO_SWITCH_ON;
			}
		
			/***If there is a rising edge at the "MoveVelocity" command, then the direction of motion and speed is assigned to output (no update needed in this case) and MoveVelocity command is given***/
			if(inst->ControlIN.Command.MoveVelocity > inst->ControlIN.Command.MoveVelocityOld)
			{	
				if (inst->ControlIN.Parameters.Direction ==FALSE)
				{
					inst->ControlOUT.Status.ControlWord		=ds402BASIC_CMD_RUN_POSITIVE;
				}
				if (inst->ControlIN.Parameters.Direction ==TRUE)
				{
					inst->ControlOUT.Status.ControlWord		= ds402BASIC_CMD_RUN_NEGATIVE;
				}
			}
			/***Check if after MoveVelocity command (ds402BASIC_CMD_RUN_POSITIVE (15) or ds402BASIC_CMD_RUN_NEGATIVE(2063)),drive changed to "ds402BASIC_ENABLED_STATUS" state ***/
			if ((inst->StatusWord | ds402BASIC_BIT_MASK_TWO) == ds402BASIC_ENABLED_STATUS )
			{
				inst->State									=ds402BASIC_OPERATION_ENABLED;
			}		
			break;
		
		case ds402BASIC_OPERATION_ENABLED:
			
			/*************************Change of command depending on selected direction of movement(rising edge on the "update" command is required)*******************************/	
			if ((inst->ControlIN.Command.Update > inst->ControlIN.Command.UpdateOld) && inst->ControlIN.Parameters.Direction==0)
			{
				inst->ControlOUT.Status.ControlWord			=ds402BASIC_CMD_RUN_POSITIVE;
			}
			if ((inst->ControlIN.Command.Update > inst->ControlIN.Command.UpdateOld) && inst->ControlIN.Parameters.Direction==1)
			{
			inst->ControlOUT.Status.ControlWord			= ds402BASIC_CMD_RUN_NEGATIVE;
			}
			/*********************Check if after command "Enable" =FALSE,drive changed to ds402BASIC_WAIT_HW_STATUS state-and then a change of state ******************************/			
			if(	inst->ControlIN.Command.Enable<inst->ControlIN.Command.EnableOld )
			{
				inst->ControlOUT.Status.ControlWord			= ds402BASIC_CMD_DISABLE;	
			}
			if ((inst->StatusWord | ds402BASIC_BIT_MASK_ONE) == ds402BASIC_WAIT_HW_STATUS)
			{
				inst->State									=ds402BASIC_WAIT_HW;
			}
			/************Check if after command ds402BASIC_CMD_ENABLE_VOLTAGE(6),drive changed to ds402BASIC_READY_TO_SWITCH_ON state-and then a change of state ******************/			
			if(inst->ControlIN.Command.SwitchOn < inst->ControlIN.Command.SwitchOnOld)
			{
				inst->ControlOUT.Status.ControlWord			= ds402BASIC_CMD_ENABLE_VOLTAGE;
			}
			if ((inst->StatusWord | ds402BASIC_BIT_MASK_TWO) == ds402BASIC_READY_SWITCH_STATUS)
			{
				inst->State									=ds402BASIC_READY_TO_SWITCH_ON;
			}
			/************Check if after command ds402BASIC_CMD_SWITCH_ON (7),drive changed to ds402BASIC_SWITCHED_ON state-and then a change of state ******************************/				
			if(inst->ControlIN.Command.MoveVelocity==FALSE)
			{
				inst->ControlOUT.Status.ControlWord			= ds402BASIC_CMD_SWITCH_ON;
			}
			if ((inst->StatusWord |  ds402BASIC_BIT_MASK_TWO) == ds402BASIC_SWITCHED_ON_STATUS)
			{
				inst->State									= ds402BASIC_SWITCHED_ON;
			}
			/************Check if after command QUICK STOP COMMAND (2),drive changed to ds402BASIC_QUICK_STOP_ACTIVE state-and then a change of state ******************************/	
			if (inst->ControlIN.Command.Stop>inst->ControlIN.Command.StopOld)
			{
				inst->ControlOUT.Status.ControlWord			= ds402BASIC_CMD_QUICK_STOP;
			}
			if ((inst->StatusWord |  ds402BASIC_BIT_MASK_TWO) == ds402BASIC_QUICK_STOP_STATUS)
			{
				inst->ControlOUT.Status.ControlWord			= ds402BASIC_CMD_DISABLE;
				inst->State									=ds402BASIC_QUICK_STOP_ACTIVE;
			}
			break;
		
		case ds402BASIC_QUICK_STOP_ACTIVE:
			/*********************************************Check if the drive is stopped - then return to the ENABLE status*************************************************************/
			if ((inst->StatusWord | ds402BASIC_BIT_MASK_ONE) == ds402BASIC_WAIT_HW_STATUS) 
			{
				inst->State									=ds402BASIC_WAIT_HW;	
			}	
			break;
	}
	/***************************************************************OUTPUT HANDLING PART**************************************************************/
	/************************************************************Check if drive is in error state*****************************************************/
	if (((inst->StatusWord | ds402BASIC_BIT_MASK_ONE)	== ds402BASIC_ERROR_STATUS) || (inst->ControlIN.Status.ModuleOk==FALSE))
	{
		inst->State											=ds402BASIC_ERROR;	
	}	
	/***********************************Check if drive is ReadyToSwitchOn and SwitchedOn -then set outputs*************************************************/
	if (((inst->StatusWord | ds402BASIC_BIT_MASK_TWO)== ds402BASIC_READY_SWITCH_STATUS) && inst->ControlIN.Command.Enable)
	{
		inst->ControlOUT.Status.SwitchedOn					=FALSE;
		inst->ControlOUT.Status.ReadyToSwitchOn				=TRUE;
	}
	else if ((inst->StatusWord | ds402BASIC_BIT_MASK_TWO)== ds402BASIC_SWITCHED_ON_STATUS)
	{
		inst->ControlOUT.Status.SwitchedOn					=TRUE;
		inst->ControlOUT.Status.ReadyToSwitchOn				=FALSE;
	}
	else if ((inst->StatusWord | ds402BASIC_BIT_MASK_TWO)== ds402BASIC_ENABLED_STATUS )
	{
		inst->ControlOUT.Status.ReadyToSwitchOn				=FALSE;
		inst->ControlOUT.Status.SwitchedOn					=TRUE;
	}
	else
	{
		inst->ControlOUT.Status.ReadyToSwitchOn				=FALSE;
		inst->ControlOUT.Status.SwitchedOn					=FALSE;
	}
	/******************************Set of FB status outputs depending on the ACOPOSInverter status**********************************/
	/******************************************Check if set point reached************************************************************/
	if ((inst->StatusWord | ds402BASIC_BIT_MASK_SET_POINT)	== ds402BASIC_ENABLED_STATUS )
	{
		inst->ControlOUT.Status.SetVelocityReached			=TRUE;
	}
	else
	{
		inst->ControlOUT.Status.SetVelocityReached			=FALSE;
	}
	/*******************************************Check if motor is moving**************************************************************/
	if (inst->State==ds402BASIC_OPERATION_ENABLED)
	{
		inst->ControlOUT.Status.InMotion						=TRUE;
	}
	else
	{
		inst->ControlOUT.Status.InMotion						=FALSE;
	}
	/*************************************Assign inst->ControlOUT structure to FB output **************************************************/
	inst->Active										=inst->ControlOUT.Status.Active;
	inst->InMotion										=inst->ControlOUT.Status.InMotion;
	inst->ControlWord									=inst->ControlOUT.Status.ControlWord;
	inst->Error											=inst->ControlOUT.Status.Error;
	inst->SetVelocityReached							=inst->ControlOUT.Status.SetVelocityReached;
	inst->ReadyToSwitchOn								=inst->ControlOUT.Status.ReadyToSwitchOn;
	inst->SwitchedOn									=inst->ControlOUT.Status.SwitchedOn;
	inst->StatusID										=inst->State;
	inst->SetVelocity									=inst->ControlOUT.Status.SetVelocity;	
	inst->SetDirection									=inst->ControlOUT.Status.SetDirection;
	/******************* If Enable = FALSE then all FB outputs are set to FALSE (Library guideline)******************/
	if (inst->ControlOUT.Status.Active==FALSE)
	{

		inst->ControlOUT.Status.InMotion					=FALSE;
		inst->ControlOUT.Status.Error						=FALSE;
		inst->ControlOUT.Status.SetVelocityReached			=FALSE;
		inst->ControlOUT.Status.ReadyToSwitchOn				=FALSE;
		inst->ControlOUT.Status.SwitchedOn					=FALSE;
		inst->ControlOUT.Status.SetVelocity					=FALSE;	
		inst->ControlOUT.Status.SetDirection				=FALSE;
	}
	
	/********************************************Edge detection*******************************************************/
	inst->ControlIN.Command.EnableOld						=inst->ControlIN.Command.Enable;
	inst->ControlIN.Command.ErrorAcknowledge				=inst->ControlIN.Command.ErrorAcknowledgeOld;
	inst->ControlIN.Command.MoveVelocityOld					=inst->ControlIN.Command.MoveVelocity;
	inst->ControlIN.Command.StopOld							=inst->ControlIN.Command.Stop;
	inst->ControlIN.Command.SwitchOnOld						=inst->ControlIN.Command.SwitchOn;
	inst->ControlIN.Parameters.DirectionOld					=inst->ControlIN.Parameters.Direction;
	inst->ControlIN.Command.UpdateOld 						=inst->ControlIN.Command.Update;
	
	

}
