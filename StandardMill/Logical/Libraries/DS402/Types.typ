
TYPE
	DS402BasicStateEnum : 
		( (*State machine enum type*)
		ds402BASIC_WAIT_HW := 0, (*Hardware initialization state*)
		ds402BASIC_QUICK_STOP_ACTIVE := 1, (*Quick stop active state*)
		ds402BASIC_OPERATION_ENABLED := 2, (*Run state*)
		ds402BASIC_SWITCHED_ON := 3, (*Switched on state*)
		ds402BASIC_READY_TO_SWITCH_ON := 4, (*Ready to switch on state*)
		ds402BASIC_ERROR := 5 (*Error state*)
		);
	DS402BasicMoveParType : 	STRUCT  (*This structure contains parameters of movement*)
		Velocity : INT; (*Set speed (rpm or Hz)*)
		DirectionOld : BOOL; (*Set direction of movement -old (used to edge detection)*)
		Direction : BOOL; (*Set direction of movement*)
	END_STRUCT;
	DS402BasicInputStatusType : 	STRUCT  (*This structure contains states of ACOPOSinverter*)
		ModuleOk : BOOL; (*ACPOPOSinverter status -TRUE/FALSE*)
		StatusWord : UINT; (*ACPOPOSinverter status - status word*)
	END_STRUCT;
	DS402BasicCommandType : 	STRUCT  (*This structure contains commands*)
		MoveVelocity : BOOL; (*Start a movement with defined speed and acceleration- edge sensitive command*)
		MoveVelocityOld : BOOL; (*Start a movement with defined speed and acceleration - old (used to edge detection)*)
		Stop : BOOL; (*Quick stop command- edge sensitive command*)
		StopOld : BOOL; (*Quick stop command- old (used to edge detection)*)
		ErrorAcknowledge : BOOL; (*Error acknowledge comand - edge sensitive command*)
		ErrorAcknowledgeOld : BOOL; (*Error acknowledge comand - old (used to edge detection)*)
		Enable : BOOL; (*The function block is active as long as this value is set*)
		EnableOld : BOOL; (*The function block is active as long as this input is set -old (used to edge detection)*)
		SwitchOn : BOOL; (*Switchtch on the voltage- edge sensitive command*)
		SwitchOnOld : BOOL; (*Switchtch on the voltage- old (used to edge detection)*)
		UpdateOld : BOOL; (*Update the parameters (velocity and direction) - old*)
		Update : BOOL; (*Updates the parameters (velocity and direction) - edge sensitive command*)
	END_STRUCT;
	DS402BasicControlnputType : 	STRUCT  (*Controln structure (main input structure)*)
		Command : DS402BasicCommandType; (*Command structure*)
		Status : DS402BasicInputStatusType; (*Status structure*)
		Parameters : DS402BasicMoveParType; (*Parameters structure*)
	END_STRUCT;
	DS402BasicOutputStatusType : 	STRUCT  (*This structure contains states of the function block output*)
		Active : BOOL; (*Indicates whether the function block is active*)
		Error : BOOL; (*Indicates that the drive is in error state*)
		InMotion : BOOL; (*Indicates whether the drive is moving*)
		ControlWord : UINT; (*Control word - command sent to ACOPOSinverter*)
		SetVelocityReached : BOOL; (*Indicates whether the drive velocity reachd the set point*)
		ReadyToSwitchOn : BOOL; (*Indicates whether the drive is ready to switched on*)
		SwitchedOn : BOOL; (*Indicates whether the drive is ready to switch on*)
		SetDirection : BOOL; (*Set direction of movement (is equal to "Direction"(input) after the update)*)
		SetVelocity : INT; (*Set poitn of movement . Is equal to "Velocity"(input) after the update*)
	END_STRUCT;
	DS402BasicControlOutputType : 	STRUCT  (*Structure of output state*)
		Status : DS402BasicOutputStatusType; (*Structure of output state*)
	END_STRUCT;
END_TYPE
