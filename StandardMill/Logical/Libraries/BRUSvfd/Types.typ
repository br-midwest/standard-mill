
TYPE
	ACPi_Model_enum : 
		(
		brusACPi_S44 := 0,
		brusACPi_X64 := 1,
		brusACPi_P74 := 2,
		brusACPi_P84 := 3,
		brusACPi_P66 := 10,
		brusACPi_P76 := 11,
		brusACPi_P86 := 12,
		brusACPi_P96 := 13
		);
END_TYPE
