
FUNCTION_BLOCK TPC (*Timer Pulse Continuous - Continuous symmetric pulse generator*)
	VAR_INPUT
		IN : BOOL; (*Function is counting when IN is high*) (* *) (*#CMD*)
		PT : TIME; (*Period of the pulse (high + low)*) (* *) (*#PAR*)
	END_VAR
	VAR_OUTPUT
		ET : TIME; (*Elapsed time*) (* *) (*#PAR*)
		Q : BOOL; (*50% Duty Square wave of length PT*) (* *) (*#PAR*)
	END_VAR
	VAR
		IS : IS_AdvTimers; (*Internal structure, do not use*) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK TOFU (*Time OFF User - Timer function with intuitive count-down display and flexible input time. User can manipulate the elements of TIMEStructure (pTS) as the timer is counting/enabled.*)
	VAR_INPUT
		IN : BOOL; (*Function is enabled when IN is high*) (* *) (*#CMD*)
		pTS : UDINT; (*Pointer to TIMEStructure used for the timer. Microseconds unused. Elements of this PV can be changed at any time, even when the timer is running.*) (* *) (*#PAR*)
		RES : BOOL; (*Clears the timer value*) (* *) (*#CMD*)
	END_VAR
	VAR_OUTPUT
		Q : BOOL; (*Q is set while TS <> 0*) (* *) (*#PAR*)
	END_VAR
	VAR
		IS : IS_AdvTimers; (*Internal structure, do not use*) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK TONR (*Retentive Time ON function block *)
	VAR_INPUT
		IN : BOOL; (*Function is enabled when IN is high*) (* *) (*#CMD*)
		PT : TIME; (*Desired count time*) (* *) (*#PAR*)
		RES : BOOL; (*Resets ET*) (* *) (*#CMD*)
	END_VAR
	VAR_OUTPUT
		ET : TIME; (*Elapsed retentive time that IN has been high since last RES*) (* *) (*#PAR*)
		Q : BOOL; (*Q is set when ET = PT*) (* *) (*#PAR*)
	END_VAR
	VAR
		IS : IS_AdvTimers; (*Internal structure, do not use*) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK TONRP (*Retentive Time ON function block with non-volitile elapsed time*)
	VAR_INPUT
		IN : BOOL; (*Function is enabled when IN is high*) (* *) (*#CMD*)
		PT : TIME; (*Desired count time*) (* *) (*#PAR*)
		RES : BOOL; (*Resets ET*) (* *) (*#CMD*)
		pET : UDINT; (*Address of TIME variable which will be used to store the elapsed time. <br><b> IMPORTANT! <br> This PV must be set as retained and permanent in order for this function to work properly!</b>*) (* *) (*#PAR*)
	END_VAR
	VAR_OUTPUT
		ET : TIME; (*Elapsed retentive time that IN has been high since last RES*) (* *) (*#PAR*)
		Q : BOOL; (*Q is set when ET = PT*) (* *) (*#PAR*)
	END_VAR
	VAR
		IS : IS_AdvTimers; (*Internal structure, do not use*) (* *) (*#OMIT*)
		inET : REFERENCE TO TIME; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK TRUN (*Timer Run. Counts up, with option to retain total time*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Enable : BOOL; (*Enables the function*) (* *) (*#PAR*)
		IN : BOOL; (*Logical condition/state for which the timer will count up*) (* *) (*#CMD*)
		RES : BOOL; (*Reset the timer*) (* *) (*#PAR*)
		pTime : REFERENCE TO UDINT; (*Address of permanent variable which is used to keep track of the cumulative run time*) (* *) (*#PAR*)
		TimeUnit : BRUS_TRUN_TimeUnit_enum; (*Largest division of time to display in output struct*) (* *) (*#PAR*)
		Update : BOOL; (*Updates the parameter inputs*) (* *) (*#PAR*)
	END_VAR
	VAR_OUTPUT
		Active : BOOL; (*True while function block is operating normally*) (* *) (*#PAR*)
		Error : BOOL; (*An error has occured, see statusID for more details*) (* *) (*#PAR*)
		StatusID : BRUS_TRUN_ID_enum; (*status number*) (* *) (*#PAR*)
		Runtime : BRUS_TRUN_Times_typ; (*Output runtime in configured format*) (* *) (*#PAR*)
	END_VAR
	VAR
		IS : BRUS_TRUN_IS; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION_BLOCK
