(*VFD Structure*)

TYPE
	VFDio_typ : 	STRUCT 
		DS402Basic_0 : DS402Basic; (*DS402Basic creating ControlWord for each drive*)
		Status : VFDstatus_typ; (*Status coming from drive in whatever units that drive uses*)
	END_STRUCT;
	VFDstatus_typ : 	STRUCT  (*Status coming from drive in whatever units that drive uses*)
		Velocity : INT;
		Torque : INT;
		Current : UINT;
		LFT_Code : INT;
		PowerRemoval : BOOL;
	END_STRUCT;
END_TYPE
