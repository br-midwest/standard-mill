
PROGRAM _INIT
	
	VFDio.DS402Basic_0.Enable := TRUE;
	
END_PROGRAM

PROGRAM _CYCLIC
	
	// Simulate DS402
	VFDio.DS402Basic_0.Simulate:= gSimulate;
	
	// Set VFDNotFound alarm
	gAlarmInfo.PersistentFlags.VFDNotFound	:= NOT VFDio.DS402Basic_0.ModuleOk;
	
	(****** ABB VFD Parameters ******)
	IF gMachine.MachineType = ACS880 THEN
		VFDio.Status.Velocity	:= DINT_TO_INT(ABBVFD_MotorSpeed);
		VFDio.Status.Current	:= DINT_TO_INT(ABBVFD_MotorCurrent);
		VFDio.Status.Torque		:= DINT_TO_INT(ABBVFD_MotorTorque);
		ABBVFD_AccelTime	:= REAL_TO_UINT(gMachine.Param.AccelTime * ABB_ACCEL_DECEL_SCALING);
		ABBVFD_DecelTime	:= REAL_TO_UINT(gMachine.Param.DecelTime * ABB_ACCEL_DECEL_SCALING);
	END_IF
	
	(****** Control the axis ******)
	// Report statuses to gMachine
	gMachine.Status.Permissives.Motion.DriveOK	:= NOT VFDio.DS402Basic_0.Error;
	gMachine.Status.ActualSpeed		:= VFDio.Status.Velocity;
	gMachine.Status.MotorCurrent	:= VFDio.Status.Current * CURRENT_SCALING;
	gMachine.Status.MotorTorquePerc	:= VFDio.Status.Torque * TORQUE_SCALING;
	gMachine.Status.ReadyToPowerOn	:= VFDio.DS402Basic_0.ReadyToSwitchOn;
	gMachine.Status.SwitchedOn		:= VFDio.DS402Basic_0.SwitchedOn;
	
	// Retrieve commands from gMachine
	VFDio.DS402Basic_0.SwitchOn			:= gMachine.Command.SwitchOn;
	VFDio.DS402Basic_0.MoveVelocity		:= gMachine.Command.MoveVelocity;
	VFDio.DS402Basic_0.ErrorAcknowledge := gAlarmList.AcknowledgeAll;
	IF gMachine.Command.DirectionREV THEN
		VFDio.DS402Basic_0.Velocity			:= -REAL_TO_INT(gMachine.Command.SetSpeed);
	ELSE
		VFDio.DS402Basic_0.Velocity			:= REAL_TO_INT(gMachine.Command.SetSpeed);
	END_IF
	
	// Update speed automatically if in RUN or JOG
	VFDio.DS402Basic_0.Update := NOT VFDio.DS402Basic_0.Update AND gMachine.MachineState <> MAIN_OFF AND gMachine.MachineState <> MAIN_ERROR;
	
	// Report any alarms the VFD gives
	IF VFDio.DS402Basic_0.Error AND VFDio.DS402Basic_0.ModuleOk AND NOT gIO.di.MachineSafe THEN
		gAlarmInfo.PersistentFlags.VFDFault := TRUE;
		// Retrieve alarm information from appropriate source, put into gAlarmInfo
		CASE gMachine.MachineType OF
			ACS880:
				brsitoa(ABBVFD_ErrorCode, ADR(gAlarmInfo.VFDErrorText) );
			BRVFD:
				// Call ACPietx()
				ACPietx(VFDio.Status.LFT_Code, brusACPi_P84, ADR(gAlarmInfo.VFDErrorText) );
		END_CASE
	ELSE
		gAlarmInfo.PersistentFlags.VFDFault := FALSE;
	END_IF
	
	// ACS880 uses Off1 when OPERATION_ENABLE is removed
	// Off1 does not allow you to restart while ramping down, which is a problem for Jog mode
	// Instead, keep MoveVelocity TRUE and set velocity to 0 if the jog button is released
	TON_JogTimeout.PT := JOG_TIMEOUT;
	IF gMachine.MachineState = MAIN_JOG THEN
		
		IF (gIO.di.JogForward OR gIO.di.JogReverse OR MoveVelocityLatch) THEN	// Latch MoveVelocity on a jog button press
			MoveVelocityLatch := TRUE;
			IF gIO.di.JogForward OR gIO.di.JogReverse THEN
				TON_JogTimeout.IN := FALSE; // Stop timer if a jog button is pressed
			ELSE
				TON_JogTimeout.IN := TRUE; // Start timer if jog button is released
				VFDio.DS402Basic_0.Velocity := 0;	// Set speed to 0 if jog button is released
			END_IF
		ELSE
			TON_JogTimeout.IN := FALSE; // Stop timer if latch is reset
		END_IF
		TON_JogTimeout();
		
		// After 5 seconds of no jog command, turn off MoveVelocity
		// After a timeout of no jog command, turn off MoveVelocityLatch
		IF TON_JogTimeout.Q THEN
			MoveVelocityLatch := FALSE;
		END_IF
		// Only use MoveVelocityLatch on ACS880
		IF gMachine.MachineType = ACS880 THEN
			VFDio.DS402Basic_0.MoveVelocity := MoveVelocityLatch;
		END_IF
	
		// If Latch turns on, give a warning horn before jogging
		TON_JogWarning.PT := JOG_WARNING;
		IF EDGEPOS(MoveVelocityLatch) THEN	// Start the timer when MoveVeloctiy is latched
			TON_JogWarning.IN := TRUE;
		END_IF
		IF TON_JogWarning.Q THEN	// Turn the warning off after the timer expires
			TON_JogWarning.IN := FALSE;
		END_IF
		IF TON_JogWarning.IN THEN						// While the timer is going
			gIO.do.Horn := TRUE;						// Sound the horn
			VFDio.DS402Basic_0.MoveVelocity := FALSE;	// Stop MoveVelocity
		ELSE						// When the timer stops
			gIO.do.Horn := FALSE;	// Stop the horn and allow MoveVelocity to be used
		END_IF
		TON_JogWarning();
		
	ELSE	// Reset MoveVelocityLatch when leaving JOG
		MoveVelocityLatch := FALSE;
	END_IF
	
	// Force OperationEnabled bit to match moveVelocity command
	VFDio.DS402Basic_0.StatusWord.2 := VFDio.DS402Basic_0.MoveVelocity;
	// Allows FUB to transition states without waiting for the drive to finish its move
	// Without this line, the drives don't update bit 2 until thier ramping operations finish.
	// DS402Basic needs bit 2 to update immediately, so it can transition in/out of OPERATION_ENABLED state.
	// If the FUB does not transition fast enough, MoveVelocity edges will be ignored.
	// Note that this is needed for both ACS880 and P84
	// BO: This may not be necessary after the Oct '22 DS402 updates, but it has not been tested on hardware
	
	// Call FUB
	VFDio.DS402Basic_0();
	
	// Add bits to control word to enable ramping on ABB VFD
	ABBVFD_ControlWord := VFDio.DS402Basic_0.ControlWord OR 2#0000_0000_0111_0000;
	
END_PROGRAM

PROGRAM _EXIT
	 
END_PROGRAM

