(*Main State Machine Structure*)

TYPE
	Machine_typ : 	STRUCT 
		MachineState : MachineState_enum;
		CoolantMode : CoolantMode_enum;
		Status : Status_typ;
		Command : Command_typ;
		Param : MachineParam_typ;
		MachineType : MachineType_enum;
	END_STRUCT;
	MachineParam_typ : 	STRUCT  (*Default values are for P84 configuration*)
		NumLightCurtains : USINT := 3;
		PullyRatioMotor : REAL := 56;
		PullyRatioShaft : REAL := 67;
		GearRatio : REAL := 15; (*[:1 ratio]*)
		RollDiameter : REAL := 6; (*[in]*)
		RatedSpeed : REAL := 1160; (*[rpm]*)
		RatedVoltage : REAL := 460; (*[V]*)
		RatedCurrent : REAL := 39; (*[A]*)
		RatedFrequency : REAL := 40; (*[Hz]*)
		RatedPower : REAL := 22.37; (*[kW]*)
		ProportionalGain : REAL := 40; (*[%]*)
		TimeIntegral : REAL := 100; (*[%]*)
		FreqencyMax : REAL := 40; (*[Hz]*)
		AccelTime : REAL := 3; (*[s]*)
		DecelTime : REAL := 0.1; (*[s]*)
	END_STRUCT;
	Status_typ : 	STRUCT 
		CoolantRunning : BOOL;
		Permissives : Permissives_typ;
		ActualSpeed : REAL;
		MotorTorquePerc : REAL;
		MotorCurrent : REAL;
		ReadyToPowerOn : BOOL;
		SwitchedOn : BOOL;
	END_STRUCT;
	Command_typ : 	STRUCT 
		CoolantStart : BOOL;
		ResetAlarms : BOOL;
		EnableForcing : BOOL;
		SoundHorn : BOOL;
		UseBtnSim : BOOL;
		PermBypass : BOOL;
		SetSpeed : REAL;
		SwitchOn : BOOL;
		MoveVelocity : BOOL;
		DirectionREV : BOOL;
	END_STRUCT;
	MachineState_enum : 
		(
		MAIN_OFF := 0,
		MAIN_WAIT := 1,
		MAIN_STARTING := 2,
		MAIN_RUN := 3,
		MAIN_JOG := 4,
		MAIN_ERROR := 5
		);
	CoolantMode_enum : 
		(
		AUTO := 0,
		MANUAL_OPTIONAL := 1,
		MANUAL_REQUIRED := 2
		);
	Permissives_typ : 	STRUCT 
		Motion : MotionPerm_typ;
		Jog : JogPerm_typ;
		Run : RunPerm_typ;
	END_STRUCT;
	MotionPerm_typ : 	STRUCT 
		DriveOK : BOOL; (*Drive has no erros*)
		PLCOK : BOOL; (*PLC has no Severity 1 or 2 alarms active*)
		CoolantOL : BOOL;
		BlowerMotorOL : BOOL;
		MotorOT : BOOL;
		DBOvertemp : BOOL;
		MotionPermsOK : BOOL;
	END_STRUCT;
	JogPerm_typ : 	STRUCT 
		InJogMode : BOOL;
	END_STRUCT;
	RunPerm_typ : 	STRUCT 
		InRunMode : BOOL;
		CoolantPumpReady : BOOL;
		LightCurtainsOK : BOOL;
		RunPermsOK : BOOL;
	END_STRUCT;
	MachineType_enum : 
		(
		SIM := 0,
		ACS880 := 1,
		BRVFD := 2
		);
END_TYPE

(*IO Channel Structure*)

TYPE
	IO_typ : 	STRUCT 
		di : di_typ;
		do : do_typ;
		ModulesOK : ARRAY[0..4]OF BOOL; (*ModuleOK for each IO module, varies for each configuration*)
	END_STRUCT;
	do_typ : 	STRUCT 
		StackLightGreen : BOOL; (*Start button ring light and green stack light*)
		StackLightAmber : BOOL; (*Amber stack light*)
		StackLightRed : BOOL; (*Red stack light*)
		Horn : BOOL;
		PLCReady : BOOL; (*PLC has no Severity 1 or 2 alarms active, used to reset E-Stop*)
		CoolantPump : BOOL;
		EstopRelay : BOOL;
	END_STRUCT;
	di_typ : 	STRUCT 
		MillStart : BOOL;
		NotMillStop : BOOL;
		RunMode : BOOL;
		JogMode : BOOL;
		JogForward : BOOL;
		JogReverse : BOOL;
		CoolantOL : BOOL; (*Safe False*)
		BlowerMotorOL : BOOL; (*Safe False*)
		CoolantOn : BOOL; (*Feedback to check that coolant pump is running*)
		BlowerOn : BOOL;
		MachineSafe : BOOL; (*Safe True, from hard-wired safety circuit, gets reset with button on button panel*)
		LightCurtain1OK : BOOL;
		LightCurtain2OK : BOOL;
		LightCurtain3OK : BOOL;
		DBOvertemp : BOOL; (*Safe True*)
		MotorOvertemp : BOOL; (*Safe True*)
	END_STRUCT;
END_TYPE

(*Alarms*)

TYPE
	AlarmInfo_typ : 	STRUCT 
		EdgeFlags : EdgeFlags_typ;
		PersistentFlags : PersistentFlags_typ;
		rxnMessage : BOOL;
		rxnControlOff : BOOL;
		rxnStopMachine : BOOL;
		ActiveAlarms : UDINT;
		PendingAlarms : UDINT;
		VFDErrorText : STRING[80];
		IOErrorText : STRING[80];
		LCNumber : USINT;
	END_STRUCT;
	PersistentFlags_typ : 	STRUCT 
		Sev1Test : BOOL;
		Sev2Test : BOOL;
		Sev3Test : BOOL;
		SafetyFault : BOOL;
		LightCurtainFault : BOOL;
		VFDNotFound : BOOL;
		VFDFault : BOOL;
		VFDCommLost : BOOL;
		CoolantOL : BOOL;
		BlowerMotorOL : BOOL;
		DBOvertemp : BOOL;
		IOFault : BOOL;
		CoolantFault : BOOL;
		MotorOvertemp : BOOL;
		BlowerFault : BOOL;
	END_STRUCT;
	EdgeFlags_typ : 	STRUCT 
		ModeChangeWhileRunning : BOOL;
		NoModeSelected : BOOL;
		JogNotAllowed : BOOL;
		CoolantRequired : BOOL;
		MotionPermsReqiured : BOOL;
		StartNotAllowed : BOOL;
		SendConfigFailed : BOOL;
		NoMachineConfig : BOOL;
		CoolantModeChanged : BOOL;
	END_STRUCT;
END_TYPE
