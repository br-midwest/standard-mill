
PROGRAM _INIT
	
	TON_Horn.PT			:= HORN_TIME;
	TPC_Light.PT		:= LIGHT_TIME;
	TPC_Light.IN		:= TRUE;
	TON_CoolantTO.PT	:= COOLANT_TIMEOUT;
	
	// Retreive configuration info
	ArProjectGetInfo_0.Execute := TRUE;
	ArProjectGetInfo_0();
	gConfigID	:= ArProjectGetInfo_0.ConfigurationID;
	gSWVersion	:= ArProjectGetInfo_0.ConfigurationVersion;
	IF DiagCpuIsSimulated() THEN
		gMachine.Command.UseBtnSim := TRUE;
		gSimulate := TRUE;
		gMachine.MachineType := SIM;	// Simulation
	ELSE
		IF brsstrcmp(ADR(gConfigID), ADR('StandardMill_ABB_FEPL02') ) = 0 THEN
			gMachine.MachineType := ACS880;	// ABB VFD
		ELSIF brsstrcmp(ADR(gConfigID), ADR('StandardMill_BR_P66') ) = 0 OR
			brsstrcmp(ADR(gConfigID), ADR('StandardMill_BR_P84') ) = 0 THEN
			gMachine.MachineType := BRVFD;	// B&R VFD
		END_IF
	END_IF
	
END_PROGRAM

PROGRAM _CYCLIC
	
	// Reset timers
	TON_Horn.IN := FALSE;
	
	(****** Permissives ******)
	// Individual motion permissives
	gMachine.Status.Permissives.Motion.PLCOK			:= gAlarmInfo.ActiveAlarms = 0;
	gMachine.Status.Permissives.Motion.CoolantOL		:= NOT gIO.di.CoolantOL;		// Safe False
	gMachine.Status.Permissives.Motion.BlowerMotorOL	:= NOT gIO.di.BlowerMotorOL;	// Safe False
	gMachine.Status.Permissives.Motion.MotorOT			:= gIO.di.MotorOvertemp;		// Safe True
	gMachine.Status.Permissives.Motion.DBOvertemp		:= gIO.di.DBOvertemp;			// Safe True
			
	// Overall motion permissive
	gMachine.Status.Permissives.Motion.MotionPermsOK 	:=	gMachine.Status.Permissives.Motion.DriveOK
		AND	gMachine.Status.Permissives.Motion.PLCOK			AND	gMachine.Status.Permissives.Motion.CoolantOL
		AND	gMachine.Status.Permissives.Motion.BlowerMotorOL	AND gMachine.Status.Permissives.Motion.MotorOT
		AND	gMachine.Status.Permissives.Motion.DBOvertemp;
	gMachine.Status.Permissives.Run.RunPermsOK := gMachine.Status.Permissives.Motion.MotionPermsOK
		AND gMachine.Status.Permissives.Run.CoolantPumpReady AND gMachine.Status.Permissives.Run.InRunMode
		AND gMachine.Status.Permissives.Run.LightCurtainsOK;
	
	// Jog permissive
	gMachine.Status.Permissives.Jog.InJogMode			:= gIO.di.JogMode;
	
	// Run Permissives
	gMachine.Status.Permissives.Run.InRunMode			:= gIO.di.RunMode;
	// Coolant
	IF gMachine.CoolantMode = MANUAL_REQUIRED THEN
		gMachine.Status.Permissives.Run.CoolantPumpReady := gMachine.Status.CoolantRunning;
	ELSE
		gMachine.Status.Permissives.Run.CoolantPumpReady := TRUE;
	END_IF
	// Light Curtains
	CASE gMachine.Param.NumLightCurtains OF
		0:
			gMachine.Status.Permissives.Run.LightCurtainsOK	:= TRUE;
		1:
			gMachine.Status.Permissives.Run.LightCurtainsOK	:= gIO.di.LightCurtain1OK;
		2:
			gMachine.Status.Permissives.Run.LightCurtainsOK	:= gIO.di.LightCurtain1OK AND gIO.di.LightCurtain2OK;
		3:
			gMachine.Status.Permissives.Run.LightCurtainsOK	:= gIO.di.LightCurtain1OK AND gIO.di.LightCurtain2OK AND gIO.di.LightCurtain3OK;
	END_CASE
	
	// Overwrite overall motion permissive if bypass command is true
	IF gMachine.Command.PermBypass THEN
		gMachine.Status.Permissives.Motion.MotionPermsOK := TRUE;
	END_IF
		
	(****** Other Statuses ******)
	gMachine.Status.CoolantRunning	:= gIO.do.CoolantPump;
	
	(****** Check for Alarms ******)
	IF gAlarmInfo.rxnControlOff OR gAlarmInfo.rxnStopMachine THEN
		gMachine.MachineState := MAIN_ERROR;
	END_IF
	
	(****** Stack Lights ******)
	gIO.do.StackLightGreen 		:= (gMachine.MachineState = MAIN_RUN) OR (gMachine.MachineState = MAIN_STARTING) OR
								(gMachine.MachineState = MAIN_WAIT AND gMachine.Status.Permissives.Motion.MotionPermsOK AND
								gMachine.Status.Permissives.Run.CoolantPumpReady AND TPC_Light.Q);
	gIO.do.StackLightAmber		:= gMachine.MachineState = MAIN_JOG;
	gIO.do.StackLightRed		:= (gMachine.MachineState = MAIN_ERROR AND TPC_Light.Q) OR NOT gIO.di.MachineSafe;
	
	(****** Main State Machine ******)
	CASE gMachine.MachineState OF
		
		MAIN_OFF:
			gMachine.Command.SwitchOn := FALSE;
			gMachine.Command.MoveVelocity := FALSE;
			// Move to MAIN_WAIT with selector switch
			IF (gIO.di.JogMode OR gIO.di.RunMode) AND gMachine.Status.ReadyToPowerOn THEN
				gMachine.MachineState := MAIN_WAIT;
			END_IF
			
		MAIN_WAIT:
			gMachine.Command.SwitchOn := TRUE;
			gMachine.Command.MoveVelocity := FALSE;
			// Wait for machine to be ready
			IF gIO.di.RunMode THEN
				IF RTrig_Start.Q THEN	// Wait for start PB
					IF NOT gMachine.Status.Permissives.Motion.MotionPermsOK THEN
						gAlarmInfo.EdgeFlags.MotionPermsReqiured:= TRUE;	// Check motion permissives
					ELSIF NOT gMachine.Status.Permissives.Run.CoolantPumpReady THEN
						gAlarmInfo.EdgeFlags.CoolantRequired := TRUE;	// Check coolant control
					ELSE
						gMachine.MachineState := MAIN_STARTING; // Enter MAIN_RUN mode if everything is good
					END_IF
				END_IF
			ELSIF gIO.di.JogMode THEN
				gMachine.MachineState := MAIN_JOG;	// Enter MAIN_JOG mode
			ELSE
				gMachine.MachineState := MAIN_OFF;	// Go back to MAIN_OFF if selector switch is turned off
			END_IF
					
		MAIN_STARTING:
			gMachine.Command.SoundHorn := TRUE;
			gMachine.Command.SwitchOn := TRUE;
			gMachine.Command.MoveVelocity := FALSE;
			TON_Horn.IN := TRUE;
			// Move on when timer expires
			IF TON_Horn.Q THEN
				gMachine.MachineState := MAIN_RUN;
			END_IF
			// Halt startup if stop button pressed
			IF NOT gIO.di.NotMillStop THEN
				gMachine.MachineState := MAIN_WAIT;
			END_IF
			// Set alarm if mode switch is changed
			IF NOT gIO.di.RunMode THEN
				gAlarmInfo.EdgeFlags.ModeChangeWhileRunning := TRUE;
			END_IF
			
		MAIN_RUN:
			// Run the VFD
			gMachine.Command.DirectionREV := FALSE;
			gMachine.Command.MoveVelocity := TRUE;
			gMachine.Command.SwitchOn := TRUE;
			// Start coolant if in AUTO mode
			IF gMachine.CoolantMode = AUTO THEN
				gMachine.Command.CoolantStart := TRUE;
			END_IF
			// Stop the machine if stop button pressed
			IF NOT gIO.di.NotMillStop THEN
				// Stop the VFD
				gMachine.Command.MoveVelocity := FALSE;
				// Stop the coolant if in AUTO mode
				IF gMachine.CoolantMode = AUTO THEN
					gMachine.Command.CoolantStart := FALSE;
				END_IF
				// Change state
				gMachine.MachineState := MAIN_WAIT;
			END_IF
			// Set alarm if mode switch is changed
			IF NOT gIO.di.RunMode THEN
				gAlarmInfo.EdgeFlags.ModeChangeWhileRunning := TRUE;
			END_IF
			// Set alarm if coolant mode changes
			IF NOT gMachine.Status.Permissives.Run.CoolantPumpReady THEN
				gAlarmInfo.EdgeFlags.CoolantModeChanged := TRUE;
			END_IF
			
		MAIN_JOG:
			// Jog the machine
			gMachine.Command.SwitchOn := TRUE;
			IF gIO.di.JogForward AND gMachine.Status.Permissives.Motion.MotionPermsOK THEN
				gMachine.Command.DirectionREV := FALSE;
				gMachine.Command.MoveVelocity := TRUE;
			ELSIF gIO.di.JogReverse AND gMachine.Status.Permissives.Motion.MotionPermsOK THEN
				gMachine.Command.DirectionREV := TRUE;
				gMachine.Command.MoveVelocity := TRUE;
			ELSE
				gMachine.Command.MoveVelocity := FALSE;
			END_IF
			// Check for a change in the mode selector
			IF gIO.di.RunMode THEN
				gMachine.MachineState := MAIN_WAIT;
			ELSIF gIO.di.JogMode THEN
				gMachine.MachineState := MAIN_JOG;
			ELSE
				gMachine.MachineState := MAIN_OFF;
			END_IF
		
		MAIN_ERROR:
			gMachine.Command.MoveVelocity	:= FALSE;
			//gMachine.Command.SwitchOn		:= FALSE;
			// Leave error state when reactions are cleared
			IF NOT gAlarmInfo.rxnStopMachine AND NOT gAlarmInfo.rxnControlOff THEN
				gMachine.MachineState := MAIN_OFF;
			END_IF
		
	END_CASE
	
	// Set remaining outputs
	gIO.do.Horn			:= gMachine.Command.SoundHorn;
	gIO.do.CoolantPump	:= gMachine.Command.CoolantStart;
	
	(****** System Timers ******)
	// PLC hours
	TRUN_Plc.Enable 	:= TRUE;
	TRUN_Plc.IN			:= TRUE;
	TRUN_Plc.pTime		:= ADR(gTRUNPlc);
	TRUN_Plc.TimeUnit	:= brusTRUN_HOURS;
	TRUN_Plc();
	gPLCTime			:= TRUN_Plc.Runtime;
	// Machine Hours
	TRUN_Machine.Enable 	:= TRUE;
	TRUN_Machine.IN			:= gMachine.MachineState = MAIN_RUN;
	TRUN_Machine.pTime		:= ADR(gTRUNMachine);
	TRUN_Machine.TimeUnit	:= brusTRUN_HOURS;
	TRUN_Machine();
	gMachineTime			:= TRUN_Machine.Runtime;
	
	(****** Alarms ******)
	// Set alarm if jog is pressed while not in MAIN_JOG mode
	IF NOT gIO.di.JogMode AND (gIO.di.JogForward OR gIO.di.JogReverse) THEN
		gAlarmInfo.EdgeFlags.JogNotAllowed := TRUE;
	END_IF
	// Set alarm if start is pressed while not in run mode
	IF NOT gIO.di.RunMode AND gIO.di.MillStart THEN
		gAlarmInfo.EdgeFlags.StartNotAllowed := TRUE;
	END_IF
	// Monitor coolant feedback input
	TON_CoolantTO.IN	:= gMachine.Command.CoolantStart;
	IF gMachine.Command.CoolantStart AND NOT gIO.di.CoolantOn AND TON_CoolantTO.Q AND NOT gMachine.Command.UseBtnSim THEN
		gAlarmInfo.PersistentFlags.CoolantFault := TRUE;
	ELSE
		gAlarmInfo.PersistentFlags.CoolantFault := FALSE;
	END_IF
	
	(****** Call FUBs ******)
	TON_Horn();
	TON_CoolantTO();
	TPC_Light();
	RTrig_Start(CLK := gIO.di.MillStart);
	
END_PROGRAM

PROGRAM _EXIT
	 
END_PROGRAM

