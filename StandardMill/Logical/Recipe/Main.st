
PROGRAM _INIT
	
	MpRecipeXml_0.Enable		:= TRUE;
	MpRecipeXml_0.MpLink		:= ADR(gRecipeXml_CFG);
	MpRecipeXml_0.FileName		:= ADR(FileName);
	MpRecipeXml_0.DeviceName	:= ADR(DeviceName);

	
	MpRecipeRegPar_0.Enable	:= TRUE;
	MpRecipeRegPar_0.MpLink	:= ADR(gRecipeXml_CFG);
	MpRecipeRegPar_0.PVName	:= ADR(PVName);
	
	TON_Save.PT := SAVE_TIME;
	
	// Set DeviceName
	IF gSimulate THEN
		DeviceName	:= 'SIM';
	ELSE
		DeviceName	:= 'USER';
	END_IF
	
	(****** Load existing recipe ******)
	REPEAT
		// Call FUBs
		MpRecipeXml_0();
		MpRecipeRegPar_0();
		
		// Wait for active
		IF MpRecipeXml_0.Active AND MpRecipeRegPar_0.Active THEN
			// Set alarm and go to cyclics if one of the FUBs has an error
			IF MpRecipeXml_0.Error OR MpRecipeRegPar_0.Error THEN
				MpRecipeXml_0.ErrorReset := TRUE;
				gAlarmInfo.EdgeFlags.NoMachineConfig := TRUE;
				EXIT;
			ELSE
				// Set Load command
				MpRecipeXml_0.Load := TRUE;
			END_IF
		END_IF
		// Go to cyclics if Load completes
		UNTIL MpRecipeXml_0.CommandDone = TRUE
	END_REPEAT
	
	// Reset Load command
	MpRecipeXml_0.Load := FALSE;
	
END_PROGRAM

PROGRAM _CYCLIC
	
	(****** Autosave Parameters ******)
	// Restart save timer if gMachine.Param and OldParam are different
	IF brsmemcmp(ADR(gMachine.Param), ADR(OldParam), SIZEOF(OldParam)) <> 0 THEN
		TON_Save(IN := FALSE);
		TON_Save(IN := TRUE);
	END_IF
	// Save when parameters are unchanged for 5 seconds
	IF TON_Save.Q THEN
		MpRecipeXml_0.Save := TRUE;
		TON_Save.IN := FALSE;
	END_IF
	// Reset save command when complete
	IF MpRecipeXml_0.CommandDone OR MpRecipeXml_0.Error THEN
		MpRecipeXml_0.Save := FALSE;
	END_IF
	// Update OldParam
	OldParam := gMachine.Param;
	
	(****** Manage FUBs ******)
	// Reset alarms
	MpRecipeXml_0.ErrorReset	:= gMachine.Command.ResetAlarms;
	MpRecipeRegPar_0.ErrorReset	:= gMachine.Command.ResetAlarms;
	
	// Call FUBs
	MpRecipeXml_0();
	MpRecipeRegPar_0();
	TON_Save();
	
END_PROGRAM

PROGRAM _EXIT
	
	MpRecipeXml_0(		Enable := FALSE);
	MpRecipeRegPar_0(	Enable := FALSE);
	
END_PROGRAM

